<div id="new_member" class="well well-small span8" style="display:none">
	<a class="close pull-right" href="#" id="closeMemFrm">&times;</a>
	<form class="form-horizontal" id="member_frm">
		<div class="control-group">
			<label class="control-label" for="firstName">First Name:</label>
			<div class="controls">
				<input type="text" id="firstName" name="firstName" class="input-xxlarge" placeholder="First Name">
			</div>
		</div>
		<div class="control-group">
			<label class="control-label" for="lastName">Last Name:</label>
			<div class="controls">
				<input type="text" id="lastName" name="lastName" class="input-xxlarge" placeholder="Last Name">
			</div>
		</div>
		<div class="control-group" id="forum_cntrl">
			<label class="control-label" for="forumName">Forum Name:</label>
			<div class="controls">
				<input type="text" id="forumName" name="forumName" class="input-xxlarge" placeholder="Forum Name" onblur="checkUsername()">
			</div>
		</div>
		<div class="control-group">
			<label class="control-label" for="pwd">Password:</label>
			<div class="controls">
				<input type="password" id="pwd" name="pwd" class="input-xxlarge" placeholder="Password">
			</div>
		</div>
		<div class="control-group">
			<label class="control-label" for="email">Email:</label>
			<div class="controls">
				<input type="text" id="email" name="email" class="input-xxlarge" placeholder="Email">
			</div>
		</div>
		<div class="control-group">
			<label class="control-label" for="homePhone">Home Phone:</label>
			<div class="controls">
				<input type="text" id="homePhone" name="homePhone" class="input-xxlarge" placeholder="Home Phone">
			</div>
		</div>
		<div class="control-group">
			<label class="control-label" for="cellPhone">Cell Phone:</label>
			<div class="controls">
				<input type="text" id="cellPhone" name="cellPhone" class="input-xxlarge" placeholder="Cell Phone">
			</div>
		</div>
		<div class="control-group">
			<label class="control-label" for="emerContact">Emergency Contact:</label>
			<div class="controls">
				<input type="text" id="emerContact" name="emerContact" class="input-xxlarge" placeholder="Emergency Contact">
			</div>
		</div>
		<div class="control-group">
			<label class="control-label" for="terms">Liability Waiver:</label>
			<div class="controls">
				<div id="waiver">
				<!-- <textarea id="terms" cols="50" rows="15" disabled="true" class="input-xxlarge"> -->
					While mountian biking is an outstanding recreational sport, it also involves risks and even dangers. Some of these dangers include but are not limited to:<br/>
					traveling on heavily rutted trails, roots, rocks-both fixed and loose, steep descents, accidents, unexpected moves of another rider, physical exertion, fatigue, and flat tires. Ride terrain, pace and distance may vary from the written or verbal description. 
					I acknowledge that Dorba encourages the wearing of helmets and agree to save and hold the Dallas Off Road Bicycle Association harmless for any injury
					resulting from my failure to wear a helmet.<br/><br/>
					I acknowledge that the risks recited above, as well as numerous other dangers are inherent in recreational bicycling and the
					undersigned agrees to assume all risks associated with participation in club activities. The undersigned further agrees to save and hold harmless Dorba, its officers, directors, coordinators, executive committee members, volunteers, other club members from any and all
					liability for any injury or damage resulting from, or in any way connected with, participation in club related activities.<br/><br/>
					I warrant that I am competent to ride safely and that my bicycle and equipment are in safe working condition. I agree to obey all traffic laws and to
					practice safety and courtesy when cycling. I hereby consent to and permit any emergency treatment in the event of injury or illness.<br/><br/>
					In the case of children under the age of 18, I hereby agree to the terms of the above waiver on behalf of my child (children). I agree to abide by
					federal, state and local helmet laws as they apply to my child (children). I agree that Dorba, its officers, activity organizers, ride leaders and other
					members have no obligation to provide instruction to, or supervision of my children.<br/><br/>
					I give to Dorba, its designees, agents and assigns, unlimited permission to use, publish and re-publish in any form or media,
					reproductions of my likeness, (photograph or video) with or without identification of me by name. I agree to not demand payment or any other
					compensation and agree to hold the above parties harmless of all liability arising from such use.<br/><br/>
					<strong>I HAVE READ AND UNDERSTAND THIS WAIVER. I AGREE TO BE LEGALLY BOUND BY IT</strong>.
				<!-- </textarea> -->
				</div>
			</div>
		</div>
		<div class="control-group">
			<div class="controls">
				<input type="checkbox" id="agree"/> I CONFIRM THAT I HAVE READ AND UNDERSTOOD THIS AGREEMENT PRIOR TO ACCEPTING IT, AND I AM AWARE THAT BY ACCEPTING THIS AGREEMENT I AM WAIVING CERTAIN LEGAL RIGHTS WHICH I OR MY HEIRS, NEXT OF KIN, EXECUTORS, ADMINISTRATORS, ASSIGNS AND REPRESENTATIVES MAY HAVE AGAINST THE RELEASEES. 
			</div>
		</div>
		<div class="control-group">
			<div class="pull-right">
				<a id="cancelMember" class="btn btn-danger">Cancel</a>
				<a id="saveMember" class="btn btn-success">Save Member</a>
			</div>
		</div>
	</form>
</div>

<!--
In consideration of membership in the Dallas Off Road Bicycle Association (hereinafter referred to as DORBA), I, for myself and my minor child/children, heirs, executors, administrators and assigns, hereby agree to forever release and discharge any and all rights, demands, claims and causes of suit or action, known or unknown, whether arising now or in the future, that I may have against DORBA, any other participating sponsors, its officers, directors, employees, representatives, agents and volunteers for any and all injuries, including death and any property damage in any manner arising or resulting from my participation or my child/children’s participation in any activity conducted by or in conjunction with DORBA.


I attest and verify that I have full knowledge of the risks involved in mountain bike riding and in all DORBA activities, that I assume those risks, that I will, without limitation, assume and pay any and all medical and emergency expenses incurred on my or my child/children’s behalf in the event of an accident, injury, illness, or other incapacity while participating in any DORBA activity, regardless of whether I have authorized such expenses. I further agree that in the event I require medical or surgical treatment while under the supervision of DORBA or any of its representatives, such DORBA representative may authorize medical treatment for myself.
-->