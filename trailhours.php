<?php include("header.php") ?>
<?php 
	//include("Connections/conn.php"); 
	
	$q = "select hours,username,trail,workDate
		from trailWork
			Inner Join users on user_id = member
			Inner Join trails on trail = trailName
		order by trail,workDate";
	$hoursQ = mysql_query($q, $conn) or die(mysql_error());
	//$hourRows = mysql_fetch_assoc($hoursQ);
	//$totalRows_channelRS = mysql_num_rows($catRS);
?>

    <div class="container-fluid">
      <div class="row-fluid">
        
        <div class="span9">
        	<ul class="breadcrumb">
			  <li>
			    <a href="/">Home</a> <span class="divider">/</span>
			  </li>
			  <li>
			    <a href="trails.php">Trails</a> <span class="divider">/</span>
			  </li>
			  <li class="active">
			  	Trail Hours
			  </li>
			  
			</ul>
			<a id="enterHours" style="display:none" class="btn btn-primary" href="trailwork.php">Enter Hours</a>
			<table class="table table-striped">
				<thead>
					<tr>
						<th></th>
						<th>Trail</th>
						<th>Worker</th>
						<th>Hours</th>
						<th>Work Date</th>
					</tr>
				</thead>
			<?php while ($hourRows = mysql_fetch_assoc($hoursQ)){ ?> 
              <tr><td></td><td><?php echo $hourRows['trail']; ?></td><td><?php echo $hourRows['username']; ?></td><td><?php echo $hourRows['hours']; ?></td><td><?php echo $hourRows['workDate']; ?></td></tr>
              <?php }  mysql_free_result($hoursQ); ?>
			</table>
        </div><!--/span-->
        <div class="span2 offest1">
          <div class="well sidebar-nav">
            <ul class="nav nav-list">
              <li class="nav-header">Trail Listing</li>
              <?php 
              	$open = "class='open'";
              	$closed = "class='closed'";
              	$status='';
              	$query_catRS = "CALL GetTrails(0);";
				$catRS = mysql_query($query_catRS, $conn) or die(mysql_error());
				$row_CatRS = mysql_fetch_assoc($catRS);
              do { 
	              if($row_CatRS['currentStatus'] == 1){ $statusClass=$open; $status = "Open";} else { $statusClass=$closed; $status="Closed";};
	              $content = "Conditions: ".$row_CatRS['conditionDesc']."<br/>Status: ".$status;
              ?> 
              <li><a <?php echo $statusClass; ?> href="trail.php?t=<?php echo $row_CatRS['trailId']; ?>" rel="popover" data-content="<?php echo $content; ?>" data-original-title="<?php echo $row_CatRS['trailName']; ?>"><?php echo $row_CatRS['trailName']; ?></a></li>
              <?php } while ($row_CatRS = mysql_fetch_assoc($catRS)); mysql_free_result($catRS); ?>       
            </ul>
            <ul class="nav nav-list" id="sponsorList">
              <li class="nav-header">DORBA Sponsors</li>
                   
            </ul>
          </div><!--/.well -->
          
        </div><!--/span-->
        
      </div><!--/row-->

      <hr>

      <footer>
        <p>&copy; DORBA 2012</p>
      </footer>

    </div><!--/.fluid-container-->
    
    <!-- Le javascript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="js/json2.js"></script>
    <script src="assets/js/jquery.js"></script>
    <script src="assets/js/bootstrap-transition.js"></script>
    <script src="assets/js/bootstrap-alert.js"></script>
    <script src="assets/js/bootstrap-modal.js"></script>
    <script src="assets/js/bootstrap-dropdown.js"></script>
    <script src="assets/js/bootstrap-scrollspy.js"></script>
    <script src="assets/js/bootstrap-tab.js"></script>
    <script src="assets/js/bootstrap-tooltip.js"></script>
    <script src="assets/js/bootstrap-popover.js"></script>
    <script src="assets/js/bootstrap-button.js"></script>
    <script src="assets/js/bootstrap-collapse.js"></script>
    <script src="assets/js/bootstrap-carousel.js"></script>
    <script src="assets/js/bootstrap-typeahead.js"></script>
    <script src="js/app.js"></script>
    <script src="js/login.js"></script>
    <script>
    	$("#sign_in").click(function(){
		   doLogin();
	   });
	
		if(readCookie("d_user") != null){
	   		u = readCookie("d_user");
	   		$("#enterHours").show();
	   		//console.log("WHAT THE: " + u);
		   buildUserPnl(JSON.parse(u));
	   } else {
	   		$("#enterHours"),hide();
		   //console.log("asdasdsad");
	   }
    </script>
  </body>
</html>
