<?php
    include("header.php");

    $query_catRS = "CALL GetRaceSeries(174);";
    $catRS = mysql_query($query_catRS, $conn) or die(mysql_error());
    $row_CatRS = mysql_fetch_assoc($catRS);
    $totalRows_channelRS = mysql_num_rows($catRS);

?>
<div class="container-fluid">

    <div class="row-fluid">
        <div class="span3 content_buffer">
            <div class="well ">
                <div id="rsvp" style="display:none"><span id="msg"><strong>Are you going?</strong></span> <a href="#" id="goto" class="btn btn-success btn-mini" onclick="rsvp(<?php echo $row_CatRS['eventId']; ?>,1)"><i class="icon-thumbs-up"></i></a> <a href="#" id="nogo" class="btn btn-danger btn-mini" onclick="rsvp(<?php echo $row_CatRS['eventId']; ?>,0)"><i class="icon-thumbs-down"></i></a></div>
                <?php if($row_CatRS['eventFlyer'] != '' || $row_CatRS['eventFlyer'] != null){ ?>
                    <img src="../<?php echo $row_CatRS['eventFlyer']; ?>" class="img-polaroid"/>
                <?php
                }
                ?>
                <img src="../files/racersclinic.jpg" class="img-polaroid"/>
                <!--
<h4>Trail Map/Location</h4>
            <div id="smallMap"></div>
            <div><address><?php echo $row_CatRS['trailAddress']; ?></address></div>
            <h4>Trail Photos</h4>
            <div id="trailImgs"></div>
-->
            </div>
        </div><!--/span-->
        <div class="span9" style="padding-top: 20px;">
<!--            <ul class="breadcrumb">-->
<!--                <li>-->
<!--                    <a href="/">Home</a> <span class="divider">/</span>-->
<!--                </li>-->
<!--                <li>-->
<!--                    <a href="events.php">Racing</a> <span class="divider">/</span>-->
<!--                </li>-->
<!--                <li class="active">-->
<!--                    --><?php //echo $row_CatRS['eventName']; ?>
<!--                </li>-->
<!--            </ul>-->

            <div class="row-fluid">
                <div class="well">
                    <div class="page-header">
                        <h3><?php echo $row_CatRS['eventName']; ?> </h3>

                    </div>
                    <h4>Race Venues</h4>
                    <table id="venues" width="100%" class="table table-striped"></table>
                </div>
            </div>
            <div class="row-fluid">
                <div class="well">
                    <?php echo $row_CatRS['eventDesc']; ?>
                </div>
            </div>
            <!--
  <div class="row-fluid">
              <table class="table table-striped table-bordered table-condensed" id="trailList_tbl">
              <thead>
                <tr>
                  <th></th>
                  <th>Name</th>
                  <th>Location</th>
                  <th>Condition</th>
                </tr>
              </thead>
              <tbody id="listBody"></tbody>
          </table>
            </div>
  --><!--/row-->
        </div><!--/span-->


    </div><!--/row-->

    <hr>

    <footer>
        <p>&copy; DORBA 2012</p>
    </footer>

</div><!--/.fluid-container-->

<!-- Le javascript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="../js/json2.js"></script>
<script src="../assets/js/jquery.js"></script>
<script src="../assets/js/bootstrap.min.js"></script>
<script src="../js/dateFormatter.js"></script>
<script src="../js/login.js"></script>
<script src="../js/raceseries.js"></script>
</body>
</html>
