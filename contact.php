<?php include("header.php") ?>
<?php 
	//include("Connections/conn.php"); 
	
	$query_catRS = "CALL GetTrails(0);";
	$catRS = mysql_query($query_catRS, $conn) or die(mysql_error());
	$row_CatRS = mysql_fetch_assoc($catRS);
	//$totalRows_channelRS = mysql_num_rows($catRS);

?>
    

    <div class="container content_buffer">
      <div class="row span8 offset2">
        
        <div class="well">
<!--        	<ul class="breadcrumb">-->
<!--			  <li>-->
<!--			    <a href="/">Home</a> <span class="divider">/</span>-->
<!--			  </li>-->
<!--			  <li class="active">-->
<!--			    <a >Contact</a> -->
<!--			  </li>-->
<!--			  -->
<!--			</ul>-->
			<h3>CONTACT US</h3>
          <form class="form-horizontal" id="contact_frm">
          <div class="control-group">
		    <label class="control-label" for="who">Who do you want to contact?</label>
		    <div class="controls">
		      <select name="who" id="who">

		      </select>
		    </div>
		  </div>
          <div class="control-group" id="email_grp">
		    <label class="control-label" for="email">Your Email</label>
		    <div class="controls">
		      <input type="text" id="email" name="email" placeholder="Your Email">
		    </div>
		  </div>
		  <div class="control-group" id="subject_grp">
		    <label class="control-label" for="subject">Subject</label>
		    <div class="controls">
		      <input type="text" id="subject" name="subject" placeholder="Subject">
		    </div>
		  </div>
		  <div class="control-group" id="msg_grp">
		    <label class="control-label" for="message">Message</label>
		    <div class="controls">
		      <textarea id="message" name="message" cols="45" rows="8"></textarea>
		    </div>
		  </div>
		  <div class="control-group">
		    <div class="controls">
		      
		      <a id="send_btn" class="btn btn-success">Send Message</a>
		    </div>
		  </div>
          </form>
			<div class="alert alert-success" style="display:none" id="message_alert">
			  Your message has been recieved! Thank you for contacting DORBA.
			</div>
        </div><!--/span-->
<!--        <div class="span2 offest1">-->
<!--          <div class="well sidebar-nav">-->
<!--            <ul class="nav nav-list">-->
<!--              <li class="nav-header">Trail Listing</li>-->
<!--              --><?php //
//              	$open = "class='open'";
//              	$closed = "class='closed'";
//              	$status='';
//              do {
//	              if($row_CatRS['currentStatus'] == 1){ $statusClass=$open; $status = "Open";} else { $statusClass=$closed; $status="Closed";};
//	              $content = "Conditions: ".$row_CatRS['conditionDesc']."<br/>Status: ".$status;
//              ?><!-- -->
<!--              <li><a --><?php //echo $statusClass; ?><!-- href="trail.php?t=--><?php //echo $row_CatRS['trailId']; ?><!--" rel="popover" data-content="--><?php //echo $content; ?><!--" data-original-title="--><?php //echo $row_CatRS['trailName']; ?><!--">--><?php //echo $row_CatRS['trailName']; ?><!--</a></li>-->
<!--              --><?php //} while ($row_CatRS = mysql_fetch_assoc($catRS)); mysql_free_result($catRS); ?><!--       -->
<!--            </ul>-->
<!--            <ul class="nav nav-list" id="sponsorList">-->
<!--              <li class="nav-header">DORBA Sponsors</li>-->
<!--                   -->
<!--            </ul>-->
<!--          </div><!--/.well -->-->
<!--          -->
<!--        </div><!--/span-->-->
        
      </div><!--/row-->
</div>
      <hr>

      <footer>
        <p>&copy; DORBA 2012</p>
      </footer>

    </div><!--/.fluid-container-->
    
    <!-- Le javascript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="assets/js/jquery.js"></script>
    <script src="assets/js/bootstrap-transition.js"></script>
    <script src="assets/js/bootstrap-alert.js"></script>
    <script src="assets/js/bootstrap-modal.js"></script>
    <script src="assets/js/bootstrap-dropdown.js"></script>
    <script src="assets/js/bootstrap-scrollspy.js"></script>
    <script src="assets/js/bootstrap-tab.js"></script>
    <script src="assets/js/bootstrap-tooltip.js"></script>
    <script src="assets/js/bootstrap-popover.js"></script>
    <script src="assets/js/bootstrap-button.js"></script>
    <script src="assets/js/bootstrap-collapse.js"></script>
    <script src="assets/js/bootstrap-carousel.js"></script>
    <script src="assets/js/bootstrap-typeahead.js"></script>
    <script src="js/contact.js"></script>
    <script src="js/login.js"></script>
</html>
