<?php include("header.php") ?>
<?php 
//	include("Connections/conn.php"); 
	
	$query_catRS = "CALL GetTrails(0);";
	$catRS = mysql_query($query_catRS, $conn) or die(mysql_error());
	$row_CatRS = mysql_fetch_assoc($catRS);
	//$totalRows_channelRS = mysql_num_rows($catRS);

?>

   

    <div class="container-fluid">
      <div class="row-fluid">
        
        <div class="span9">
        	<ul class="breadcrumb">
			  <li>
			    <a href="/">Home</a> <span class="divider">/</span>
			  </li>
			  <li class="active">
			    <a >Welcome</a> 
			  </li>
			  
			</ul>
          <div id="content-content" class="content-content">
                                  
<div id="node-6616" class="node odd full-node node-type-book">
  <div class="inner node-inner">
    
    
    
    
    <div class="content clearfix node-content">
      <p>
</p><h1>
	<span class="Apple-style-span" style="font-size:12px;">Compiled by Jeanne Patterson and Geoffrey Rogers</span></h1>
<p>
	The Dallas Off Road Bicycle Association (DORBA), a nonprofit organization, was founded in 1988 by a small group of off-road enthusiasts who wanted to share the thrills of this new sport with others in the community. DORBA is currently one of the largest off-road bicycle clubs in the country with over 650 members. The first two years of existence were spent promoting membership in the club, and promoting two nationally sanctioned races as an avenue to raise funds. Since the Dallas/Ft. Worth area had no public riding areas at this time, DORBA was forced to hold all events on privately owned property. Realizing this was a major drawback in the growth of the sport, the club began to look for opportunities in the area to rectify this situation.</p>
<p>
	<b>1990</b> The Grapevine Lake Corps of Engineers allow DORBA to use one of their hiking trails and to extend this trail along the north side of the lake. Begin the "Superdude" awards program to encourage bike club members to work on trail development and trail maintenance. During the first year of this program we logged 138 hours of volunteer labor at the Northshore Trail.</p>
<p>
	<b>1991</b> Join the City of Dallas Park &amp; Recreation Department to develop the L. B. Houston Hike &amp; Bike Trail. Begin talk with the Collin County Open Space Program on developing a trail system at Sister Grove Park. Start monthly trail maintenance and development program at the Northshore Trail. Receive "Volunteer of the Month" award from the City of Dallas for work on the L. B. Houston Hike &amp; Bike Trail. Build bicycle racks at the Dallas Nature Center. Present program on the benefits gained by off-road bicycle trails to Recreation and Park specialists at the Texas Trails Symposium. Northshore Trail receives National Recreation Trail Status - Corps of Engineers credits DORBA's work. Contribute 600 hours in volunteer labor to these projects.</p>
<p>
	<b>1992</b> Finalist in the Department of Interior's "Take Pride in America" program for work at the Northshore National Recreation Trail. Construct the Sister Grove Park Trail on Collin County Open Space Property. Join the City of Dallas Parks &amp; Recreation Department to develop the Boulder Park Trail. Begin Trail Patrol at the Northshore National Recreation Trail. Continue monthly trail maintenance and trail construction at area parks. Logged 630 hours in volunteer labor to these projects. Organized and hosted the first annual D/FW Mountain Bike Championship'series utilizing local trails, all built and maintained by DORBA. Race was attended by over 300 racers and raised $4,000 to benefit local trail projects.</p>
<p>
	<b>1993</b> Enter agreement with Cedar Hill State Park to develop a mountain bike trail in the park. Dedicated this trail on June 5th, National Trails Day. Begin work with the City of Grapevine on Horseshoe Trail at City Park. Hold Second Annual D/FW Mountain Bike Championship Series - over 500 racers participate. Receive special recognition from the Texas Recreation and Parks Society for work on the L. B. Houston Hike &amp; Bike Trail. Received the National Volunteer Service Award from the National Recreation and Parks Association presented in October at their national convention. Begin talks with Dallas County Open Space to develop trails in 10 of their parks. Continue Trail Patrol at the Northshore National Recreation Trail. Receive Volunteer of the Year Award - Civic Group from the Volunteer Council of Plano. Continue monthly trail maintenance and trail construction at area parks. Logged over 1300 hours in volunteer labor to these projects.</p>
<p>
	<b>1994</b> Complete 3-mile extension to the Sister Grove Park Trail and 5-mile extension to the Cedar Hill State Park Trail. Celebrate the completion of both of these projects during the weekend of National Trails Day. Over 120 bicyclists and numerous hikers attended these events, held in opposite parts of the Dallas Metroplex. Continue monthly trail maintenance and trail construction at area parks. Logged over 1500 hours in volunteer labor to these projects. Hold Third Annual D/FW Mountain Bike Championship Series in 9/94. Produce the "Evian Ride for the Wild" - a mountain bicycling rally benefiting the World Wildlife Fund. Over 500 cyclists attended. Start first Explorer Post Program in conjunction with the Boy Scouts of America. Begin work with BSA to develop a mountain bike program for their summer camps.</p>
<p>
	<b>1995</b> Enter agreement with Dallas County Open Space to build trails at Rowlett Creek Preserve and at Windmill Hill Preserve. Continue trail maintenance and trail construction projects. Logged over 1600 hours in volunteer labor to these projects. Hold Fourth Annual D/FW Mountain Bike Championship Series in 9/95. Host Second Evian "Ride for the Wild" at Cedar Hill State Park 4/95. Receive Model Club Award from the International Mountain Bicycling Association 9/95.</p>
<p>
	<b>1996</b> Continue trail maintenance and trail construction projects. Logged over 1500 hours in volunteer labor to these projects. Hold Fifth Annual D/FW Mountain Bike Championship Series in 9/96. Hold Womenís Only Mountain Biking Clinic/Retreat 8/96. Receive $8,800 grant from the National Recreational Trails Grant Program.</p>
<p>
	<b>1997</b> Enter agreement with City of Plano to construct trail at Arbor Hills Nature Preserve, a new City of Plano Park. Enter agreement with City of McKinney to construct trail at Erwin Park. Enter agreement with Texas Parks &amp; Wildlife Department to construct trail at the Johnson Branch of the Ray Roberts State Park. Add 3-mile extension to Cedar Hill State Park Trail. Host 2nd. Women's Only Mountain Biking Clinic/Retreat 8/97. Log 2,035.3 hours in volunteer trail building/maintenance projects. Hold Sixth Annual D/FW Mountain Bike Championship Series - Fall 1997. DORBA Board Member Jeanne Patterson presented "Action Hero Award" - one of 11 Americans recognized by the <a href="http://www.orca.org/IMBA/"><b>International Mountain Bicycling Association</b></a> for role in advancing trail advocacy, volunteerism and open space.</p>
<p>
	<b>1998</b> DORBA built new trails at Arbor Hills Nature Preserve and Erwin Park. Cedar Hill Challenge was rained out for a second year. Got approval to start building trail at Johnson Branch SP. DORBA continues to maintain local trails. A total of 1656 hours of volunteer trail work during 1998. DORBA hosted the Clearfork Festival on Memorial Day Weekend at Camp Clearfork in Cystal Springs, AR. Host 3rd Women's Only Mountain Biking Clinic/Retreat in August. Hold the 7th D/FW Championship Series in the fall. Annouced that DORBA was moving it race in the Texas Championship series to The Breaks @ Bar-H Ranch in Saint Jo, Texas.</p>
<p>
	<b>1999</b> Finished all of the planned trails at Erwin Park. Completed the first 3 mile section of single track at Johnson Branch. Log 1726 hours in volunteer trail building/maintenance projects. Hold the first Bar-H Bash Mountain Bike Race at The Breaks @ Bar-H Ranch (it didn't rain). Host the Clearfork Festival on Memorial Day Weekend at Camp Clearfork in Cystal Springs, AR. Host 4th Women's Only Mountain Biking Clinic/Retreat in August. Hold the 8th D/FW Championship Series in the fall.</p>
<p>
	<b>2000</b> Logged 2397.5 hours in volunteer trail building/maintenance projects. Hold the 2nd Bar-H Bash Mountain Bike Race at The Breaks @ Bar-H Ranch. Host the Clearfork Festival on Memorial Day Weekend at Camp Clearfork in Cystal Springs, AR. Host 4th Women's Only Mountain Biking Clinic/Retreat in August. Hold the 9th D/FW Championship Series in the fall. Collin County made plans to turn Sister Grove Park into a Youth Day Camp.</p>
<p>
	<b>2001</b> Completed making a single track loop at Johnson Branch. This made the trail roughly 9 miles. Started the TOTM (Trail of the Month) program. Started talking with Isle du Bois SP about building a trail there. Logged 3044 hours in volunteer trail building/maintenance projects. Hold the 3rd Bar-H Bash Mountain Bike Race at The Breaks @ Bar-H Ranch. Had the first Spokes for Folks Wrenchday. Host the Clearfork Festival on Memorial Day Weekend at Camp Clearfork in Cystal Springs, AR. Host 5th Women's Only Mountain Biking Clinic/Retreat in August. Hold the 10th D/FW Championship Series in the fall. Collin County plans to turn Sister Grove Park into a Youth Day Camp fell through.</p>
<p>
	<b>2002</b> Logged 4049 hours in volunteer trail building/maintenance projects. Started talking with the City of Arlington about building a trail at River Legacy Park. Enter into an agreement with the City of Frisco to build a trail on city park land at 4th Army and Lebonan in Frisco. Started building the first mile of trail at Isle du Bois SP. Hold the 4th Bar-H Bash Mountain Bike Race at The Breaks @ Bar-H Ranch. Started the Tour de Dallas program (to encourage people to ride more and different DORBA trails). Hosted the Clearfork Festival on Memorial Day Weekend at Camp Clearfork in Cystal Springs, AR. Hold the 11th D/FW Championship Series in the fall. Collin County Officals approval a $230,000 improvement project for Sister Grove Park.</p>
<p>
	<b>2003</b> Logged 5266 hours in volunteer trail building/maintenace projects. Completed the following trail projects River Legacy, Arlington, Hawk Hills, Frisco and Isle du Bois, Pilot Point. Hold the 5th Bar-H Bash Mountain Bike Race at the Breaks @ Bar-H Ranch. Hosted the Clearfork Festival on Memorial Day Weekend at Camp Clearfork in Cystal Springs, AR. Hold the 12th D/FW Championship Series on the following trails Rowlett Creek, Cedar Hill SP and Sister Grove. The Sister Grove improvement project got started by Collin County.</p>
<p>
	<b>2004</b> Logged 3780 hours in volunteer trail building/maintenace projects. Made Tawakoni State Park and Cross Timbers trails as DORBA supported trails. Held the 6th Bar-H Bash Mountain Bike Race at the Breaks @ Bar-H Ranch. Hosted Women's Only Mountain Biking Clinic/Retreat in August. Hosted the Clearfork Festival on Memorial Day Weekend at Camp Clearfork in Cystal Springs, AR. Held the 13th D/FW Championship Series on the following trails L.B.Houston, Erwin Park and Johnson Branch SP. The Collin County completed tge Sister Grove improvement project.</p>
<p>
	&nbsp;</p>
<p>
	<b>2005</b> Logged 9212 hours in volunteer trail building/maintenace projects. Made Eisenhower State Park a DORBA supported trail. Completed the first phase of restoring Cedar Hill SP trail system after damage caused by the 500 year flood in 2004. Hosted Women's Only Mountain Biking Clinic/Retreat in August. Held the 7th Bar-H Bash Mountain Bike Race at the Breaks @ Bar-H Ranch. Hosted the Clearfork Festival on Memorial Day Weekend at Camp Clearfork in Cystal Springs, AR. Held the 14th D/FW Championship Series on the following trails Rowlett Creek, Cedar Hill SP and Sister Grove. Third year in a roll with no rain outs or rain date. The DFW RCP race set new one race DFW Series attendence record with 444 racers. Set new DFW Series attendences record with 1053 racers for the three races. Held the 1st Frozen Four Hour Series. The Frozen Four Hour series is a low key four hour endurance race series. Held the series at Rowlett Creek and Johnson Branch.</p>
<p>
	&nbsp;</p>
<p>
	<b>2006</b> Logged 4275 hours in volunteer trail building/maintenace projects. Hosted Women's Only Mountain Biking Clinic/Retreat in August. Held the 8th Bar-H Bash Mountain Bike Race at the Breaks @ Bar-H Ranch. Hosted the Clearfork Festival on Memorial Day Weekend at Camp Clearfork in Cystal Springs, AR. Held the 15th D/FW Championship Series on the following trails L.B.Houston, Erwin Park and Johnson Branch. Fourth year in a roll with no rain outs or rain date. Set new DFW Series attendences record with 1107 racers for the three races. Held the 2nd Frozen Four Hour Series. Held the series at the following venues: Boulder Park and Isle du Bois.</p>
<p></p>
  <div id="book-navigation-4453" class="book-navigation">
    
        <div class="page-links clear-block">
              <a href="/event/2011/2011-frozen-series-6-hour-endurance-race-2nd-3" class="page-previous" title="Go to previous page">‹ 2011 Frozen Series 6-hour Endurance Race (2nd of 3) </a>
                    <a href="/book/dorba-archives" class="page-up" title="Go to parent page">up</a>
                    <a href="/forum/racing/8797" class="page-next" title="Go to next page">Dirty Kanza 200 Race Report ›</a>
          </div>
    
  </div>
    </div>

    
        <div class="links">
      <ul class="links inline"><li class="comment_forbidden first last"><span><a href="/user/login?destination=comment%2Freply%2F6616%23comment-form">Login</a> or <a href="/user/register?destination=comment%2Freply%2F6616%23comment-form">register</a> to post comments</span></li>
</ul>    </div>
    

  
  </div><!-- /inner -->
</div><!-- /node-6616 -->
                                                                  </div>

        </div><!--/span-->
        <div class="span2 offest1">
          <div class="well sidebar-nav">
            <ul class="nav nav-list">
              <li class="nav-header">Trail Listing</li>
              <?php 
              	$open = "class='open'";
              	$closed = "class='closed'";
              	$status='';
              do { 
	              if($row_CatRS['currentStatus'] == 1){ $statusClass=$open; $status = "Open";} else { $statusClass=$closed; $status="Closed";};
	              $content = "Conditions: ".$row_CatRS['conditionDesc']."<br/>Status: ".$status;
              ?> 
              <li><a <?php echo $statusClass; ?> href="trail.php?t=<?php echo $row_CatRS['trailId']; ?>" rel="popover" data-content="<?php echo $content; ?>" data-original-title="<?php echo $row_CatRS['trailName']; ?>"><?php echo $row_CatRS['trailName']; ?></a></li>
              <?php } while ($row_CatRS = mysql_fetch_assoc($catRS)); mysql_free_result($catRS); ?>       
            </ul>
            <ul class="nav nav-list" id="sponsorList">
              <li class="nav-header">DORBA Sponsors</li>
                   
            </ul>
          </div><!--/.well -->
          
        </div><!--/span-->
        
      </div><!--/row-->

      <hr>

      <footer>
        <p>&copy; DORBA 2012</p>
      </footer>

    </div><!--/.fluid-container-->
    
    <!-- Le javascript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="assets/js/jquery.js"></script>
    <script src="assets/js/bootstrap-transition.js"></script>
    <script src="assets/js/bootstrap-alert.js"></script>
    <script src="assets/js/bootstrap-modal.js"></script>
    <script src="assets/js/bootstrap-dropdown.js"></script>
    <script src="assets/js/bootstrap-scrollspy.js"></script>
    <script src="assets/js/bootstrap-tab.js"></script>
    <script src="assets/js/bootstrap-tooltip.js"></script>
    <script src="assets/js/bootstrap-popover.js"></script>
    <script src="assets/js/bootstrap-button.js"></script>
    <script src="assets/js/bootstrap-collapse.js"></script>
    <script src="assets/js/bootstrap-carousel.js"></script>
    <script src="assets/js/bootstrap-typeahead.js"></script>
    <script src="js/app.js"></script>
  </body>
</html>
