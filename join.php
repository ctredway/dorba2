<?php 
	//error_reporting(E_ERROR);
	include("Connections/conn.php"); 
	
	$query_catRS = "CALL GetTrails(0);";
	$catRS = mysql_query($query_catRS, $conn) or die(mysql_error());
	$row_CatRS = mysql_fetch_assoc($catRS);
	//$totalRows_channelRS = mysql_num_rows($catRS);

?>
    <?php include("header.php") ?>

    <div class="container content_buffer">
      <div class="row span8 offset1">
        
        <div class="span9">
<!--        	<ul class="breadcrumb">-->
<!--			  <li>-->
<!--			    <a href="/">Home</a> <span class="divider">/</span>-->
<!--			  </li>-->
<!--			  <li class="active">-->
<!--			    <a >Join</a> -->
<!--			  </li>-->
<!--			  -->
<!--			</ul>-->
          
            <div id="start" class="well well-large span8">
            	<div class="alert">If you think your membership is still valid, please email <a href="mailto:join.dorba@gmail.com">join.dorba@gmail.com</a></div>
				<h2>
					WELCOME!</h2>
				<p >
					We are glad you are interested to learn more about <strong>DORBA</strong> and how to become an active member.</p>
				<p>
					If you already have an account on our forums, please log in now and you will be presented with our membership options. 
				</p>
				<p>
					If you do NOT have an account with us, the please click <a class="btn btn-primary btn-mini" id="showStart">JOIN</a> to create one. Once that is completed you will be presented with our membership options.
				</p>
				<p>
					If you choose to pay online, please make sure you are logged into the site. Logging in will ensure that your transaction will be successful, as the PayPal link is integrated with our site coding.
				</p>
				<p>
					We look forward to hearing from you.</p>
				
				<p>
				<strong>Membership Cost:</strong>
				<br/><br/>
				<strong>Individual Membership</strong> ......$25.00
				     <p>With processing fee - $26.03<br/>
				Regular annual membership for one person. (1 vote)</p>
				
				<strong>Family Membership</strong> ...........$30.00
				     <p>With processing fee - $31.17<br/>
				Provides membership for up to two adults and all minor children in a single household. (2 votes)</p>
				
				<strong>Junior Membership</strong> ............$10.00
				    <p> With processing fee - $10.59<br/>
				Membership for youth aged 13-17 years. Parent or Guardian's contact information must be included in order for this membership to be processed. (No vote)</p>
				
				<strong>Corporate Membership</strong> ......$75.00
				    <p> With processing fee - $77.48<br/>
				Membership for up to five adults within an organization. (1 vote)</p>
				</p>
            </div>
            <?php include('member_form.php'); ?>
            <?php include('choose_level.php'); ?>
        </div><!--/span-->
<!--        <div class="span2 offest1">-->
<!--          <div class="well sidebar-nav">-->
<!--            <ul class="nav nav-list">-->
<!--              <li class="nav-header">Trail Listing</li>-->
<!--              --><?php //
//              	$open = "class='open'";
//              	$closed = "class='closed'";
//              	$status='';
//              do {
//	              if($row_CatRS['currentStatus'] == 1){ $statusClass=$open; $status = "Open";} else { $statusClass=$closed; $status="Closed";};
//	              $content = "Conditions: ".$row_CatRS['conditionDesc']."<br/>Status: ".$status;
//              ?><!-- -->
<!--              <li><a --><?php //echo $statusClass; ?><!-- href="trail.php?t=--><?php //echo $row_CatRS['trailId']; ?><!--" rel="popover" data-content="--><?php //echo $content; ?><!--" data-original-title="--><?php //echo $row_CatRS['trailName']; ?><!--">--><?php //echo $row_CatRS['trailName']; ?><!--</a></li>-->
<!--              --><?php //} while ($row_CatRS = mysql_fetch_assoc($catRS)); mysql_free_result($catRS); ?><!--       -->
<!--            </ul>-->
<!--            <ul class="nav nav-list" id="sponsorList">-->
<!--              <li class="nav-header">DORBA Sponsors</li>-->
<!--                   -->
<!--            </ul>-->
<!--          </div><!--/.well -->-->
<!--          -->
<!--        </div><!--/span-->-->
        
      </div><!--/row-->
</div>
      <hr>

      <footer>
        <p>&copy; DORBA 2012</p>
      </footer>

    </div><!--/.fluid-container-->
    
    <!-- Le javascript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="js/json2.js"></script>
    <script src="assets/js/jquery.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
    <script src="js/login.js"></script>
    <script src="js/join.js"></script>
  </body>
</html>
