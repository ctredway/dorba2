<?php include("header.php") ?>
<?php 
	//include("Connections/conn.php"); 
	
	$query_catRS = "CALL GetTrails(0);";
	$catRS = mysql_query($query_catRS, $conn) or die(mysql_error());
	$row_CatRS = mysql_fetch_assoc($catRS);
	//$totalRows_channelRS = mysql_num_rows($catRS);

?>

    <div class="container-fluid">
      <div class="row-fluid">
        
        <div class="span9">
        	<ul class="breadcrumb">
			  <li>
			    <a href="/">Home</a> <span class="divider">/</span>
			  </li>
			  <li class="active">
			    <a >Welcome</a> 
			  </li>
			  
			</ul>
          
            <p><h4>DORBA Vision Statement</h4>
The Dallas Off-Road Bicycle Association seeks to be at the forefront of promoting the sport of mountain biking, land access, and advocacy for the North Texas area, and serve as a model of leadership for other cycling organizations.
<br/><br/>
<h4>DORBA Mission Statement</h4>

<strong>DORBA will seek to accomplish its Vision through:</strong>
<br/>
–Cycling education programs that advance a cyclists knowledge and skill level in a fun and encouraging atmosphere;<br/>
–Non-competitive cycling events that foster a sense of fellowship and camaraderie that appeals to the diversity that is found within mountain biking community;<br/>
–Competitive cycling events catering to XC, endurance, and racers of all racing skill levels;<br/>
–Adherence to national IMBA guidelines for trail development and other insurance requirements;<br/>
–Open, direct, and legal partnerships with land managers that can serve as models for new trails<br/>

            </p>
<div class="span12"><a href="files/dorba_vision.pdf" target="_blank" class="offset10"><img src="images/pdf-icon.png" alt="Download PDF version" title="Download PDF version"/></a><a href="join.php" class="btn btn-success pull-right">Join Now &raquo;</a></div>
        </div><!--/span-->
        <div class="span2 offest1">
          <div class="well sidebar-nav">
            <ul class="nav nav-list">
              <li class="nav-header">Trail Listing</li>
              <?php 
              	$open = "class='open'";
              	$closed = "class='closed'";
              	$status='';
              do { 
	              if($row_CatRS['currentStatus'] == 1){ $statusClass=$open; $status = "Open";} else { $statusClass=$closed; $status="Closed";};
	              $content = "Conditions: ".$row_CatRS['conditionDesc']."<br/>Status: ".$status;
              ?> 
              <li><a <?php echo $statusClass; ?> href="trail.php?t=<?php echo $row_CatRS['trailId']; ?>" rel="popover" data-content="<?php echo $content; ?>" data-original-title="<?php echo $row_CatRS['trailName']; ?>"><?php echo $row_CatRS['trailName']; ?></a></li>
              <?php } while ($row_CatRS = mysql_fetch_assoc($catRS)); mysql_free_result($catRS); ?>       
            </ul>
            <ul class="nav nav-list" id="sponsorList">
              <li class="nav-header">DORBA Sponsors</li>
                   
            </ul>
          </div><!--/.well -->
          
        </div><!--/span-->
        
      </div><!--/row-->

      <hr>

      <footer>
        <p>&copy; DORBA 2012</p>
      </footer>

    </div><!--/.fluid-container-->
    
    <!-- Le javascript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="assets/js/jquery.js"></script>
    <script src="assets/js/bootstrap-transition.js"></script>
    <script src="assets/js/bootstrap-alert.js"></script>
    <script src="assets/js/bootstrap-modal.js"></script>
    <script src="assets/js/bootstrap-dropdown.js"></script>
    <script src="assets/js/bootstrap-scrollspy.js"></script>
    <script src="assets/js/bootstrap-tab.js"></script>
    <script src="assets/js/bootstrap-tooltip.js"></script>
    <script src="assets/js/bootstrap-popover.js"></script>
    <script src="assets/js/bootstrap-button.js"></script>
    <script src="assets/js/bootstrap-collapse.js"></script>
    <script src="assets/js/bootstrap-carousel.js"></script>
    <script src="assets/js/bootstrap-typeahead.js"></script>
    <script src="js/login.js"></script>
    <script src="js/app.js"></script>
  </body>
</html>
