
    <?php include("header.php") ?>


      <div class="row-fluid">
          <div class="span3 hidden-phone sidebar-nav" id="left_nav">
              <div><!-- empty row --><img src="images/shim.png" height="40"/> </div>
              <div class="well sidebar-nav">
                  <h4 class="text-center">THE TRAILS</h4>
                  <ul class="nav nav-list" id="trailList1">

                  </ul>
             </div>
              <div class="well sidebar-nav">
                  <h4 class="text-center">UPCOMING EVENTS</h4>
                  <ul class="nav nav-list" id="eventArea">

                  </ul>
              </div><!--/.well -->

          </div><!--/span-->
        <div class="span9" id="main_body">
            <div><!-- empty row --><img src="images/shim.png" height="40"/></div>
        	<div class="alert alert-danger">
        	<!-- make this more prominent --> 
			  If you are NEW or even a RETURNING member, please head to our forums <a href="/forum">here</a> and create an account.
			</div>
          <div class="well">
            <h2 class="hidden-phone">Welcome to DORBA</h2>
            <h3 class="hidden-desktop hidden-tablet">Welcome to DORBA!</h3>
            <p>Welcome to the Dallas Off Road Bicycle Association web site.  Our primary mission is providing access to great local trails.  Your membership provides the tools and infrastructure used to build trails. It also lets local municipalities know that you care about and use the trails on their properties. Whether you are an experienced rider or someone who is just starting out, we hope you’ll find the site full useful information.
	        <br/><br/>Please browse around. A good place to start is the forum and the calendar and we encourage you join us on a ride soon. You do not have to be a DORBA Member to attend most rides/clinics.  We hope to see you on the trail soon!

            </p>
            
            <p><a href="detail.php" class="btn btn-large">View &raquo;</a>  <a href="join.php" class="btn btn-large">Join Now &raquo;</a></p>
          </div>
<!--          <div class="container">-->
            <div id="newsArea" class="well">
                <div id="articles"><h4>From the President</h4><br/></div>
            </div>
<!--	          <div class="row-fluid">-->
<!--	          		<div id="eventArea" class="span8">-->
<!--	          			-->
<!--	          		</div>-->
<!--		          <div id="dorbaStats" class="span4">-->
<!--		          	-->
<!--		          	--><?php ////include('member.php'); ?>
<!--		          </div>-->
	          </div>
          </div>
<!--           -->
<!--        </div><!--/span-->-->

        
      </div><!--/row-->

      <hr>

      <footer>
        <p>&copy; DORBA 2012</p>
              </footer>

    </div><!--/.fluid-container-->
    <div class="modal hide" id="phoneLogin_frm">
    	<div id="mhead" class="modal-header">Login</div>
    	<div class="modal-body">
	    	<form id="login_frm2">
			  <input type="text" name="u" placeholder="User Name" class="input-small"/> <input type="password" name="p" placeholder="Password" class="input-small"/>
			  
			</form>
		</div>
		<div class="modal-footer"><a id="sign_in2" class="btn btn-primary btn-mini">Login</a></div>
    </div>
        <!-- Le javascript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->

    <script src="js/json2.js"></script>
    <script src="assets/js/jquery.js"></script>
    <script src="js/dateFormatter.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>

    <script src="js/login.js"></script>
    <script src="js/app.js"></script>
    
  </body>
</html>
