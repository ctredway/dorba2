$(function(){
	//start up code
	$("#addEvent").click(function(){
		showNewEvent();
	});
	
	$("#saveEvent").click(function(){
		publishEvent();
	});
});

btn = '<button type="button" class="close pull-right" data-dismiss="modal" aria-hidden="true" id="closeMe" onclick="closeMe()">×</button>';

function deleteArticle(event){
	$.get('../services/deletearticle.php?d='+event,function(data){
		window.location = 'articles.php';
	});
}

function closeMe(){
	$("#newEventFrm").fadeToggle("fast", "linear");
	$("#sidebar").fadeToggle("slow", "linear");
	$("#middle").fadeToggle("slow", "linear");
}

function showNewEvent(){
	$("#hdr").html('New Article '+btn);
	clearForm();
	$(".jquery_ckeditor").ckeditor();
	$("#newEventFrm").fadeToggle("fast", "linear");
	$("#sidebar").fadeToggle("slow", "linear");
	$("#middle").fadeToggle("slow", "linear");
}

function clearForm(){
	$("#eventId").val('');
	$("#eventName").val('');
	$("#eventStart").val('');
	$("#eventEnd").val('');
	$("#editor1").val('');
	$("#shortDesc").val('');
	$("#homepage").val(0);
}

function editArticle(event){
	
	clearForm();
	//$( "#eventEnd" ).datepicker();
	$.getJSON('../services/article.php?e='+event,function(data){
		//console.log(data.articles[0].articld);
		var e = data.articles[0].article;
		$("#hdr").html(e.articleTitle+ ' ' +btn);
		$("#articleid").val(e.articleId);
		$("#title").val(e.articleTitle);
		$("#editor1").val(e.articleText);
		//console.log(e.showOnHomePage);
		
		$(".jquery_ckeditor").ckeditor();
		$("#newEventFrm").fadeToggle("fast", "linear");
		$("#sidebar").fadeToggle("slow", "linear");
		$("#middle").fadeToggle("slow", "linear");
	});
}

function showImages(event){
	$("#eventfiles_modal").modal('show');
	$.getJSON('../services/eventfiles.php?e='+event,function(data){
		
	});
}

function addEventFile(){
	var fileToUse = "";
	
	//if(which == "top"){
		fileToUse = "services/uploadtrailimage.php";	
	//} 
	
	//alert(which+'_slideimage ' + fileToUse);

	$.ajaxFileUpload
	(
		{
			url:fileToUse,
			secureuri:false,
			fileElementId:'trail_photo',
			dataType: 'json',
			data:{trail:tt, id:'id'},
			success: function (data, status)
			{
				if(typeof(data.error) != 'undefined')
				{
					if(data.error != '')
					{
						alert("ERROR: " + data.error);
					}else
					{
						
						//if(which == "top"){
							//alert(data.msg);
							//$('#trailImgs').empty();
							$('#trailImgs').append("<img src='images/trails/"+data.msg+"'/>");
							image = "<img src='images/trails/"+data.msg+"'/>";
							//alert($('#topContent').val());
						//} 
					}
				}
			},
			error: function (data, status, e)
			{
				alert(e);
			}
		}
	)
	
	return false;
}

function closeFiles(){
	$("#eventfiles_modal").modal('hide');
}

function publishEvent(){
	
	$.post('../services/savearticle.php',$("#newevent_frm").serialize(),function(data){
		window.location='articles.php';
		//console.log(data);
	});
	
}
