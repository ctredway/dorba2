var userList = [];
$(function(){
	$(".jquery_ckeditor").ckeditor();
	$("#addSponsor").click(function(){
		newSponsor();
		$("#sponsorList_tbl").hide();
	});
	
	$("#saveSponsorBtn").click(function(){
		saveSponsor();
	});
	
	$("#saveMgr").click(function(){
		assignUser();
	});
	
	$("#cancelMgr").click(function(){
        clearAdminForm();
		$("#sponsorMgr").hide();
		$("#sponsorList_tbl").show();

        $("#sponsor_tbl").hide();
        $("#sponsorAdmins").empty();
	});
	
	$("#closeMe").click(function(){
		hideMe('sponsorForm');
	});
	
	getForumUsers();
	
	grabSponsors();
});

var close = '<button type="button" class="close pull-right" aria-hidden="true" id="closeMe" onclick="hideMe()">×</button>';

function editSponsor(sponsor){
	$.getJSON("../services/sponsors.php?s="+sponsor,function(data){
		
		newSponsor();
		var s = data.sponsors[0].sponsor;
		$("#hdr").html('Edit '+s.sponsorName+close);
		$("#sid").val(s.sponsorId);
		$("#sponsorName").val(s.sponsorName);
		$("#website").val(s.sponsorUrl);
		$("#contact").val(s.contact);
		$("#editor1").val(s.sponsorDesc);
		//$("#sponsorForm").show();
		
	});
}

function saveSponsor(){
	
	$.post('../services/savesponsor.php',$("#sponsor_frm").serialize(),function(data){
		$("#sponsorList_tbl").show();
		grabSponsors();
		newSponsor();
	});
}

function clearAdminForm(){
    $("#spid").val(0);
    $("#uid").val(0);
    $("#sponsor").val('');
}

function newSponsor(){
	
	$("#hdr").html('New Sponsor'+close);
	$("#sid").val('0');
	$("#sponsorName").val('');
	$("#website").val('');
	$("#editor1").val('');
	$("#contact").val('');
	if( $('#sponsorForm').is(':visible') ) {
		
		$("#sponsorForm").hide();
	} else {

    	// it's not visible so do something else
    	$("#sponsorForm").show();
    }
}

function removeSponsor(sponsor){
    $.get('../services/deletesponsor.php?s='+sponsor,function(data){
        grabSponsors();
        hideMe('sponsorForm');
    });
}

function assignUser(){
//    TODO: add table and service to assing a user
    $.post('../services/assignuser.php',$("#sponsor_admin_frm").serialize(),function(data){

        getSponsorAdmins($("#spid").val());
    });
}

function hideMe(what){
	if(!what){
		what = 'sponsorForm';
	}
	$("#"+what).hide();
}

function grabSponsors(){
	$.getJSON('../services/sponsors.php',function(data){
		$("#sponsorlistBody").empty();
		$.each(data.sponsors, function(i,item){
			$("#sponsorlistBody").append('<tr><td><a href="#" class="btn btn-warning btn-mini" onclick="showAssign('+item.sponsor.sponsorId+')";><i class="icon-user"></i></a> <a href="#" onclick="editSponsor('+item.sponsor.sponsorId+')"; class="btn btn-success btn-mini"><i class="icon-edit icon-white"/></a> <a href="#" class="btn btn-danger btn-mini" onclick="removeSponsor('+item.sponsor.sponsorId+');"><i class="icon-remove icon-white"></i></a></td><td><a href="../sponsor.php?s='+item.sponsor.sponsorId+'">'+item.sponsor.sponsorName+'</a></td><td>'+item.sponsor.sponsorUrl+'</td></tr>');
		});
	});
}

function getForumUsers(){
	$.getJSON('../services/getforumusers.php',function(data){
		userList = [];
		$("#sponsor").val('');
		
		$.each(data.members, function(i,item){
			
			userList.push({"label":item.member.username,"value":item.member.user_id});
		});

		$( "#sponsor" ).autocomplete({
            minLength: 2,
            source: userList,
            focus: function( event, ui ) {
                $( "#sponsor" ).val( ui.item.label );
                return false;
            },
            select: function( event, ui ) {
                //console.log(ui.item.value);
                $("#uid").val(ui.item.value);
                return false;
            }
        });
	});
	
}

function removeAssign(_usr,sp){
    $.get('../services/removeassign.php?u='+_usr+'&s='+sp,function(data){
        getSponsorAdmins(sp);
    });
}

function setUser(item){
	console.log(item);
}

function getSponsorAdmins(sp){
    $.getJSON('../services/getsponsoradmins.php?s='+sp,function(data){
        $("#sponsorAdmins").empty();
        $.each(data.members,function(i,item){
            $("#sponsorAdmins").append('<tr><td><a href="#" class="btn btn-small btn-danger" onclick="removeAssign('+item.member.user_id+','+sp+');"><i class="icon-remove"></i></a> </td><td>'+item.member.username+'</td><td>'+item.member.user_email+'</td></tr>');
        });
        $("#sponsor_tbl").show();
    });
}

function showAssign(sp){

    $("#spid").val(sp);
    getSponsorAdmins(sp);
	$("#sponsorList_tbl").hide();
	$("#sponsorMgr").show();
}