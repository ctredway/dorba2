$(function(){
	$("#editMember").hide();
   
   $("#sign_in").click(function(){
	   doLogin();
   });
   
   $("#sign_in2").click(function(){
   	   $("#phoneLogin_frm").modal('hide');
	   doPhoneLogin();
   });
   
   $("#showPhoneLogin_btn").click(function(){
	   showPhoneLogin();
   });
   
   $("#saveMember").click(function(){
		$.post('services/updatemember.php',$("#member_frm").serialize(),function(data){
			//console.log(data);
			$("#editMember").hide();
			window.location.reload(true);
		})
	});
	
	$("#saveContact").click(function(){
		$.post('services/updatecontact.php',$("#contact_frm").serialize(),function(data){
			//usr = JSON.parse(u);
			getContacts(uid);
			//console.log(data);
			$("#editContact").hide();
			//window.location.reload(true);
		})
	});
	
	$("#showEdit_btn").click(function(){
		$("#memDisplay").hide();
		$("#editMember").show();
	});
	
	$("#cancelMember1").click(function(){
		$("#memDisplay").show();
		$("#editMember").hide();
	});
	
	$("#cancelMember").click(function(){
		$("#choose").hide();
	});
	
	$("#cancelContact").click(function(){
		$("#editContact").hide();
	});
	
	$("#renew").click(function(){
		$("#choose").show();
		$("#cancelMember").show();
	});
	
	$("#add_contact").click(function(){
		$("#cc").attr('value',0);
		$("#uu").attr('value',_usr.user_id);
        $("#cfirstName").attr('value','');
        $("#clastName").attr('value','');
        //$("#cfirstName").attr('value',item.contact.firstName);
        $("#crelationship").attr('value','');
        $("#cforumname").val('');
        $("#cforumId").val(0);
		$("#editContact").show();
	});
	
	setPrice();
    getContacts(uid);
});
function getContacts(usr){
	$.getJSON('services/getcontacts.php?u='+usr+'&c=0', function(data){
		$("#mContact_rows").empty();
		$.each(data.contacts, function(i,item){
			$("#mContact_rows").append('<tr><td><a href="#" onclick="editContact('+item.contact.memberContactId+');" class="btn btn-success btn-mini"><i class="icon-edit"></i></a> <a href="#" class="btn btn-danger btn-mini" onclick="removeContact('+item.contact.memberContactId+');"><i class="icon-remove-circle"></i></a> </td><td>'+item.contact.firstName + ' ' + item.contact.lastName+'</td><td>'+item.contact.relationship+'</td></tr>');
		});
		$("#mContacts").show();
	});
}

function editContact(contact){
	usr = uid;
	$.getJSON('services/getcontacts.php?u='+uid+'&c='+contact, function(data){
		//console.log(data.contacts);
        getForumUsers();
		$.each(data.contacts, function(i,item){
			usr = uid;
			$("#cfirstName").attr('value',item.contact.firstName);
			$("#clastName").attr('value',item.contact.lastName);
			//$("#cfirstName").attr('value',item.contact.firstName);
			$("#crelationship").attr('value',item.contact.relationship);
			$("#cc").attr('value',item.contact.memberContactId);
			$("#uu").attr('value',uid);
            if(item.contact.forumId != 0){
                $.getJSON('services/getforumusers.php?u='+item.contact.forumId,function(data){
                   // console.log(data.members[0].member.username);
                    $("#cforumname").val(data.members[0].member.username);
                    $("#cforumId").val(data.members[0].member.user_id);
                });
            }

		});

		$("#editContact").show();
	});
}

function getForumUsers(){
    $.getJSON('services/getforumusers.php',function(data){
        userList = [];
       // $("#cforumname").val('');

        $.each(data.members, function(i,item){

            userList.push({"label":item.member.username,"value":item.member.user_id});
        });

        $( "#cforumname" ).autocomplete({
            minLength: 2,
            source: userList,
            focus: function( event, ui ) {
                $( "#cforumname" ).val( ui.item.label );
                return false;
            },
            select: function( event, ui ) {
                //console.log(ui.item.value);
                $("#cforumId").val(ui.item.value);
                return false;
            }
        });
    });
}

function removeContact(contact){
    $.get('services/deletemembercontact.php?c='+contact,function(data){
       getContacts(_usr.user_id);
    });
}

function setPrice(){
	option = $("#memberType").val();
	//console.log(option);
	switch(option){
		case 0:
			
			break;
		case "1":
			$("#memberCost").val('26.03');
			$("#memberDisplay").val('Individual');
			//console.log($("#memberCost").val());
			break;
		case "2":
			$("#memberCost").val('31.17');
			$("#memberDisplay").val('Family');
			break;
		case "3":
			$("#memberCost").val('10.59');
			$("#memberDisplay").val('Junior');
			break;
		case "4":
			$("#memberCost").val('77.48');
			$("#memberDisplay").val('Corporate');
			break;
	}
}