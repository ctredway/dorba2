var _gaq = _gaq || [];
_gaq.push(['_setAccount', 'UA-39059824-1']);
_gaq.push(['_trackPageview']);

//var basepath = 'http://localhost/dorba';
var basepath = '';

(function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);

    //console.log('UID: ' + uid);

    if(uid && uid != 1){
        $.get(basepath+'/services/autologin.php?u='+uid,function(data){
            u = JSON.parse(data);
            createCookie("d_user",data,2);
            buildUserPnl(u);
        });
    }
})();

function logout(){
	eraseCookie("d_user");
	window.location = 'http://www.dorba.org';
}

function doLogin(){
	var rr = new Date().getTime();
	$.post(basepath+'/services/login.php?rr='+ rr,$("#login_frm").serialize(),function(data){
		if(data != "0" && data != "" && data != null && data != 0){
			u = JSON.parse(data);
			createCookie("d_user",data,2);
			buildUserPnl(u);
			readCookie("d_user");
		} else {
			alert('Your login is incorrect, please try again');
		}
		
	});
}

function doPhoneLogin(){
	var rr = new Date().getTime();
	$.post(basepath+'/services/login.php?rr='+ rr,$("#login_frm2").serialize(),function(data){
		if(data != "0" && data != "" && data != null && data != 0){
			u = JSON.parse(data);
			createCookie("d_user",data,2);
			buildPhoneUserPnl(u);
			readCookie("d_user");
		} else {
			alert('Your login is incorrect, please try again');
		}
		 
	});
}

function buildUserPnl(u){

	$("#show_user_name").empty().html("Welcome, "+u.username);
	$("#login_div").hide();
	$("#usr_prof").append('<a href="profile.php?u='+u.user_id+'">My Profile</a>');
    $("#usr_evnt_mnu").append('<a href="myevents.php?u='+u.user_id+'">My Events</a>');
	$("#profile_mnu").show();
	var rr = new Date().getTime();
	$.post(basepath+'/services/checkfortrails.php?t='+u.user_id+'&'+rr,function(data){
		if(data != 0){
			eraseCookie("d_steward1");
			eraseCookie("d_steward");
			createCookie("d_steward1",data,8);
		}
	});
}

function buildPhoneUserPnl(u){
	$("#show_user_name").empty().html(u.username);
	$("#usr_prof").append('<a href="profile.php?u='+u.user_id+'">My Profile</a>');
	$("#profile_mnu").show();
	var rr = new Date().getTime();
	$.post(basepath+'/services/checkfortrails.php?t='+u.user_id+'&'+rr,function(data){
		if(data != 0){
			eraseCookie("d_steward1");
			eraseCookie("d_steward");
			createCookie("d_steward1",data,8);	
		}
	});
	$("#showPhoneLogin_btn").hide();
}

//cookie functions
function createCookie(name,value,days) {
    var expires;
    if (days) {
        var date = new Date();
        date.setTime(date.getTime()+(days*24*60*60*1000));
        expires = "; expires="+date.toGMTString();
    }
    else expires = "";
    document.cookie = name+"="+value+expires+"; path=/";
}

function readCookie(name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    
    for(var i=0;i < ca.length;i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') c = c.substring(1,c.length);
        //console.log(c);
        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
    }
    return null;
}

function eraseCookie(name) {
    createCookie(name,"",-1);
}