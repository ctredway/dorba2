$(document).ready(function() {

    $("#home").removeClass("active");
    $("#trails").removeClass("active");
    $("#forums").removeClass("active");
    $("#events").addClass("active");
    $("#sponsors").removeClass("active");

    if(readCookie("d_user") != null){
        u = readCookie("d_user");
        //console.log("WHAT THE: " + u);
        if( /Android|webOS|iPhone|iPod|BlackBerry/i.test(navigator.userAgent) ) {

            buildPhoneUserPnl(JSON.parse(u));
            getMyEvents(JSON.parse(u));
        } else {

            buildUserPnl(JSON.parse(u));
            getMyEvents(JSON.parse(u));
        }

    } else {
        //console.log("asdasdsad");
        $("#eventList").hide();
    }


    $("#sign_in").click(function(){
        doLogin();
    });

    $("#sign_out").click(function(){
        logout();
    });
    
    $("#add_evt").click(function(){
        //console.log(u.user_id);
    	//_usr = JSON.parse(u);
	    $(".jquery_ckeditor").ckeditor();
	    $("#newEventFrm").show();
	    $("#eventList").hide();
	    buildDatePickers();
	    $("#usr").val(u.user_id);
    });
    
    $("#saveEvent").click(function(){
	  	$.post('services/saveevent.php', $("#newevent_frm").serialize(),function(){
		  		//alert(u);
		  	getMyEvents(u);
		  	$("#eventList").show();
		  	$("#newEventFrm").hide();
		  	
	  	});
    });

});

function deleteEvent(event){
	$.get('services/deleteevent.php?d='+event,function(data){
		//_usr = JSON.parse(u);
		getMyEvents(u);
	});
}

function showAddImage(e){
	$("#addImg").modal('show');
	$("#eid").val(e);
}

//gets users event list
function getMyEvents(usr){
	_usr = usr.user_id;
	$.getJSON('services/getuserevents.php?u='+_usr, function(data){
		$("#eventData").empty();
		$.each(data.events, function(i,item){
			$("#eventData").append('<tr><td><a href="#" class="btn btn-primary btn-mini" onclick="editEvent('+item.event.eventId+')"><i class="icon-edit"></i></a> <a href="#" class="btn btn-primary btn-mini" title="Add Event Flyer" onclick="showAddImage('+item.event.eventId+');"><i class="icon-picture icon-white"></i></a> <a href="#" class="btn btn-danger btn-mini" onclick="deleteEvent('+item.event.eventId+')"><i class="icon-remove"></i></a></td><td>'+item.event.eventName+'</td><td>'+item.event.eventStart+'</td></tr>');
		});
	});
}

function editEvent(event){
	$.getJSON('services/events.php?e='+event,function(data){
		//console.log(data.events[0].event.eventId);
		_usr = u;
		$("#usr").val(_usr.user_id);
		var e = data.events[0].event;
		$("#hdr").html(e.eventName);
		$("#eventId").val(e.eventId);
		$("#eventName").val(e.eventName);
		$("#eventStart").val(e.eventStart);
		$("#eventEnd").val(e.eventEnd);
		$("#shortDesc").val(e.eventShortDesc);
		$("#eventTime").val(e.eventTime);
		$("#eReg").val(e.regUrl);
		$("#editor1").val(e.eventDesc);
		$(".jquery_ckeditor").ckeditor();
		$("#eventList").hide();
		$("#newEventFrm").fadeToggle("fast", "linear");
		buildDatePickers();
		$("#homepage").val(e.showOnHomePage);
	});
}

function addEventFile(){
	//console.log($("#eid").val());
	var fileToUse = "";
	
	fileToUse = "services/uploadeventflyer.php";	
	
	$.ajaxFileUpload
	(
		{
			url:fileToUse,
			secureuri:false,
			fileElementId:'eventImg',
			dataType: 'json',
			data:{id:$("#eid").val()},
			success: function (data, status)
			{
				if(typeof(data.error) != 'undefined')
				{
					if(data.error != '')
					{
						alert("ERROR: " + data.error);
					}else
					{
						
						//if(which == "top"){
							//alert(data.msg);
							//$('#trailImgs').empty();
							$('#img').append("<img src='../"+data.msg+"'/>");
							$("#img").show();
							image = "<img src='images/trails/"+data.msg+"'/>";
							//alert($('#topContent').val());
						//} 
					}
				}
			},
			error: function (data, status, e)
			{
				alert(e);
			}
		}
	)
	
	return false;
}


function buildDatePickers(){
	$( "#eventStart" ).datepicker({
		onSelect: function(data,instance){
			$( "#eventStart" ).datepicker( "option", "dateFormat", 'yy-mm-dd' );
		}
	});
	
	$( "#eventEnd" ).datepicker({
		onSelect: function(data,instance){
			$( "#eventEnd" ).datepicker( "option", "dateFormat", 'yy-mm-dd' );
		}
	});
}