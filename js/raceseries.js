$(function(){
    $("#home").removeClass("active");
    $("#trails").removeClass("active");
    $("#forums").removeClass("active");
    $("#events").addClass("active");
    $("#sponsors").removeClass("active");

    $("#sign_in").click(function(){
        doLogin();
        u = readCookie("d_user");
        __usr=JSON.parse(u);

        $("#rsvp").show();

        checkForRsvp(174,__usr.user_id);

    });

    if(readCookie("d_user") != null){
        u = readCookie("d_user");
        __usr=JSON.parse(u);
        //console.log("WHAT THE: " + u);
        buildUserPnl(JSON.parse(u));
        $("#rsvp").show();

        checkForRsvp(174,__usr.user_id);

    } else {
        //console.log("asdasdsad");
        $("#rsvp").hide();
    }

    getRaceVenues(184);
});

function getUrlVars() {
    var vars = {};
    var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(m,key,value) {
        vars[key] = value;
    });
    return vars;
}

function checkForRsvp(evt,uid){
    $.get('../services/checkrsvp.php?u='+uid+'&e='+evt,function(data){
        console.log(data);
        $("#msg").empty();
        if(data == 'na'){
            $("#goto").show();
            $("#nogo").show();
            $("#msg").append('<strong>Are you going?</strong> ');
        } else if(data == 0){
            $("#goto").show();
            $("#nogo").hide();
            $("#msg").append('<strong>Are you going?</strong> No');
        } else if(data == 1){
            $("#goto").hide();
            $("#nogo").show();
            $("#msg").append('<strong>Are you going?</strong> Yes');
        }
    });
}

function rsvp(evt,going){
    _usr = JSON.parse(u);
    $.get('../services/rsvp.php?u='+_usr.user_id+'&e='+evt+'&g='+going, function(data){
        //console.log(going);
        $("#msg").empty();

        if(data == 0){
            $("#goto").show();
            $("#nogo").hide();
            $("#msg").append('<strong>Are you going?</strong> No');
        } else if(data == 1){
            $("#goto").hide();
            $("#nogo").show();
            $("#msg").append('<strong>Are you going?</strong> Yes');
        }

    });
}

function getRaceVenues(race){
    $.getJSON('../services/racevenues.php?e='+race,function(data){

        $.each(data.events,function(i,item){
            var d = new Date(item.event.start);
            var now = new Date();
            var url = 'Register';
            if(item.event.regUrl != ''){
                url = '<a href="'+item.event.regUrl+'" target="_blank">Register</a>';
            }
            if(d < now && item.event.regUrl != ''){
                url = '<a href="'+item.event.regUrl+'" target="_blank">Results</a>';
            }
           $("#venues").append('<tr><td width="140"></td><td><a href="../event.php?e='+item.event.id+'">'+item.event.title+'</a></td><td>'+ (d.getMonth()+1) +'/'+(d.getDate()+1) +'/' + d.getFullYear() +'</td><td>'+url+'</td></tr>');
        });
    });
}

