$(function(){
	var opt = {"placement":"left"};
	$("a[rel=popover]").popover(opt).click(function(e) {
        //e.preventDefault()
        //alert('what');
      });
    grabTrails();
    grabSponsors();
    getHomePageEvents();
    getNews();

   $("#home").addClass("active");
   $("#trails").removeClass("active");
   $("#forums").removeClass("active");
   $("#events").removeClass("active");
   $("#sponsors").removeClass("active");

   $("#sign_in").click(function(){
	   doLogin();
   });

   $("#sign_in2").click(function(){
   	   $("#phoneLogin_frm").modal('hide');
	   doPhoneLogin();
   });

   $("#showPhoneLogin_btn").click(function(){
	   showPhoneLogin();
   });

   if(readCookie("d_user") != null){
   		u = readCookie("d_user");
   		//console.log("WHAT THE: " + u);
   		if( /Android|webOS|iPhone|iPod|BlackBerry/i.test(navigator.userAgent) ) {
			buildPhoneUserPnl(JSON.parse(u));
			$("#phoneLogin").hide();
		} else {
			buildUserPnl(JSON.parse(u));
		}

   } else {
	   //console.log("asdasdsad");
   }

});

function showPhoneLogin(){
	$("#phoneLogin_frm").modal();
	$("#phoneLogin_frm").modal('show');
}

function showQuickTrailInfo(trail){

}

function grabTrails(){
    var rr = new Date().getTime();
    $.getJSON('services/trailList.php?'+ rr,function(data){
        $.each(data.categories, function(i,item){
            //console.log(i);
            $("#trailList").append('<li class="nav-header">'+item.category.name+'</li>');
            $.each(item.category.trails[0], function(i,item){
              //  console.log(item.trail);
                var cls = 'open';
                if(item.trail.currentStatus == 0){
                    cls = 'closed';
                }
                $("#trailList").append('<li><a class="'+cls+'" href="trail.php?t='+item.trail.trailId+'">'+item.trail.trailName+'</a></li>');
            });
        });
    });

}

function grabSponsors(){
	var rr = new Date().getTime();
	$.getJSON('services/sponsors.php?'+ rr,function(data){
		$.each(data.sponsors, function(i,item){
			$("#sponsorList").append('<li><a class="sponsor" href="sponsor.php?s='+item.sponsor.sponsorId+'">'+item.sponsor.sponsorName+'</a></li>');
		});
	});

}

function getHomePageEvents(){
	var rr = new Date().getTime();
	$.getJSON('services/homepageevents.php?'+rr,function(data){
		var events = data.events;
		buildEvents(events);
	});
}

function getNews(){
	var rr = new Date().getTime();
	$.getJSON('services/article.php?'+rr,function(data){
		var articles = data.articles;
		buildNews(articles);
	});
}

function buildNews(newsList){
	$.each(newsList, function(i,item){
		$("#articles").append('<div><strong>'+item.article.articleTitle+'</strong><br/>'+item.article.articleText+'</div>');
	});
}

function buildEvents(evtList){

	begin = '<div class="row-fluid">';
	end = "</div>";
	html = "";
	//evt = [];
	for(i=0;i<evtList.length;i++){
		evt = evtList[i].event;
		//console.log(event);
		if(i==0){
			//html += begin;
		}
		var img = '';
		if(evt.eventFlyer !== '' && evt.eventFlyer !== null && evt.eventFlyer !== 'null'){
			img = '<img style="height:80px;width:60px" src="'+evt.eventFlyer+'"/>';
		}

        var et = '';
        if(evt.eventTime != '' && evt.eventTime != null){
            et = ' @ ' + evt.eventTime;
        }

        html += '<li><a href="event.php?e='+evt.eventId+'">'+img+evt.eventName+'</a></li>';
        //html += '<div><h4>'+img+evt.eventName+'</h4><strong>'+ evt.eventStart2 +'</strong>'+ et +' <br/>' +evt.eventShortDesc+'<p><a class="btn" href="event.php?e='+evt.eventId+'">View event &raquo;</a></p></div>';
		if(i % evtList.length == 2){
			//html += end+begin;
		}
	}
	//html += end;
	//console.log(html);
	$("#eventArea").append(html);
}