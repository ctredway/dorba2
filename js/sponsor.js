$(function(){
    $("#home").removeClass("active");
    $("#trails").removeClass("active");
    $("#forums").removeClass("active");
    $("#events").removeClass("active");
    $("#sponsors").addClass("active");

    $("#sign_in").click(function(){
        doLogin();
    });

    $("#edit_sponsor").click(showEditForm);
    $("#saveEdit").click(function(){
        $.post('services/savesponsor.php',$("#sponsor_edit_frm").serialize(),function(data){
            $("#sponsor_txt").html($("#sponsor_html").val());
            $("#sponsor_edit").hide();
            $("#sponsor_txt").show();
        });
    });

    if(readCookie("d_user") != null){
        u = readCookie("d_user");
        uu = JSON.parse(u);
        //console.log("WHAT THE: " + u);
        buildUserPnl(JSON.parse(u));
        checkForAdmin(uu.user_id);
    } else {
        //console.log("asdasdsad");
    }

    function checkForAdmin(uid){
        $.getJSON('services/checksponsoradmin.php?u='+uid+'&s='+getUrlVars()['s'],function(data){
            if(data != false){
                $("#edit_sponsor").show();
            } else {
                $("#edit_sponsor").hide();
            }
        });
    }

    function showEditForm(){
        $("#sponsor_txt").hide();
        $("#sponsor_html").ckeditor();
        $("#sponsor_html").val($("#sponsor_txt").html());
        $("#sponsor_edit").show();
    }

    function getUrlVars() {
        var vars = {};
        var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(m,key,value) {
            vars[key] = value;
        });
        return vars;
    }
});
