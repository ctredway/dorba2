$(function(){
	//startup
	$("#home").removeClass("active");
   $("#trails").removeClass("active"); 
   $("#forums").removeClass("active");
   $("#events").removeClass("active");
   $("#sponsors").removeClass("active");
   $("#contact").removeClass("active");
   $("#join").addClass("active");

    if(readCookie("d_user") != null){
        u = readCookie("d_user");
        uu = JSON.parse(u);

        if( /Android|webOS|iPhone|iPod|BlackBerry/i.test(navigator.userAgent) ) {
            buildPhoneUserPnl(JSON.parse(u));
            $("#choose").show();
            $("#start").hide();
            $("#memberId").val(uu.user_id);
        } else {

            buildUserPnl(JSON.parse(u));
            $("#choose").show();
            $("#start").hide();
            $("#memberId").val(uu.user_id);
        }

    } else {
        //console.log("asdasdsad");
    }
   
	$("#showStart").click(function(){
		$("#new_member").show();
	});
	
	$("#closeMemFrm").click(function(){
		$("#new_member").hide();
	});
	
	$("#sign_in").click(function(){
		doLogin();
	});
	
	$("#saveMember").click(function(){
		if($("#agree").attr("checked")){
			$.post('services/savemember.php',$("#member_frm").serialize(),function(data){
				//console.log(data);
				$("#new_member").hide();
				$("#choose").show();
                $("#start").hide();
				$("#memberId").val(data);
				$("#memberType").val(1);
			});
		} else {
			alert('You must agree to the terms to join DORBA');
		}
	});
	
	$("#memberCost").val('26.03');
	$("#memberDisplay").val('Individual');
	
});

function setPrice(){
	option = $("#memberType").val();
	//console.log(option);
	switch(option){
		case 0:
			
			break;
		case "1":
			$("#memberCost").val('26.03');
			$("#memberDisplay").val('Individual');
			//console.log($("#memberCost").val());
			break;
		case "2":
			$("#memberCost").val('31.17');
			$("#memberDisplay").val('Family');
			break;
		case "3":
			$("#memberCost").val('10.59');
			$("#memberDisplay").val('Junior');
			break;
		case "4":
			$("#memberCost").val('77.48');
			$("#memberDisplay").val('Corporate');
			break;
	}
}

function checkUsername(){
	userName = $("#forumName").val();
	$.get('../services/checkusername.php?u='+userName,function(data){
		if(data == 1){
			$("#forum_cntrl").addClass('error');
			$("#forum_cntrl").removeClass('success');
			alert("that name is taken");
		} else if(data == 0){
			$("#forum_cntrl").addClass('success');
			$("#forum_cntrl").removeClass('error');
		}
	});
}

function logout(){
	//alert("erasing");
	eraseCookie("d_user");
	window.location = window.URL;
}

function doLogin(){
	var rr = new Date().getTime();
	$.post('services/login.php?'+ rr,$("#login_frm").serialize(),function(data){
		//alert("!"+data+"!");
		if(data != "0" && data != "" && data != null && data != 0){
			u = JSON.parse(data);
			createCookie("d_user",data,2);
			buildUserPnl(u);
			//console.log(data);
			readCookie("d_user");
			$("#memberId").val(u.user_id);
			$("#choose").show();
		} else {
			alert('Your login is incorrect, please try again');
		}
		
	});
}

function buildUserPnl(u){
	//console.log(u);
	//u = JSON.parse(usr);
	$("#show_user_name").empty();
	$("#show_user_name").html("Welcome, "+u.username);
	$("#login_div").hide();
	$("#profile_mnu").show();
	var rr = new Date().getTime();
	$.post('services/checkfortrails.php?t='+u.user_id+'&'+rr,function(data){
		//console.log(JSON.parse(data));
		if(data != 0){
			eraseCookie("d_steward1");
			eraseCookie("d_steward");
			createCookie("d_steward1",data,8);	
		}
		
	});
}

//cookie functions
function createCookie(name,value,days) {
    if (days) {
        var date = new Date();
        date.setTime(date.getTime()+(days*24*60*60*1000));
        var expires = "; expires="+date.toGMTString();
    }
    else var expires = "";
    document.cookie = name+"="+value+expires+"; path=/";
}

function readCookie(name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    
    for(var i=0;i < ca.length;i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') c = c.substring(1,c.length);
        //console.log(c);
        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
    }
    return null;
}

function eraseCookie(name) {
    createCookie(name,"",-1);
}