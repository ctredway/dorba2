<?php include("header.php") ?>
<?php
//include("Connections/conn.php");

$query_catRS = "CALL GetTrails(0);";
$catRS = mysql_query($query_catRS, $conn) or die(mysql_error());
$row_CatRS = mysql_fetch_assoc($catRS);
$totalRows_channelRS = mysql_num_rows($catRS);

?>
<div class="container-fluid">
    <ul class="breadcrumb">
        <li>
            <a href="index2.php">Home</a> <span class="divider">/</span>
        </li>
        <li>
            <a href="profile.php?u=<?php echo $_REQUEST['u']; ?>">Profile</a> <span class="divider">/</span>
        </li>
        <li class="active">
            My Events
        </li>
    </ul>
    <div class="page-header">
        <h1>My Events</h1>
    </div>
    <div class="row">

        <div class="span8">
	        <table class="table table-bordered table-striped " id="eventList">
	          <thead>
	            <tr>
	            	<th><a href="#" id="add_evt" class="btn btn-success btn-mini">Add Event</a></th>
	              <th>Event</th>
	              <th>Starts</th>
	            </tr>
	          </thead>
	          <tbody id="eventData">
	          </tbody>
	        </table>
        </div><!--/span-->

        <div id="newEventFrm" style="display:none;">
		    	<div class="modal-header" id="hdr">New Event</div>
		    	<div>
		    		<form class="form-horizontal" id="newevent_frm">
		    			<input type="hidden" name="eventId" id="eventId" value="0"/>
		    			<input type="hidden" name="usr" id="usr" value="0"/>
					  <div class="control-group">
					    <label class="control-label" for="eventName">Event Name</label>
					    <div class="controls">
					      <input type="text" id="eventName" name="eventName" placeholder="">
					    </div>
					  </div>
					  <div class="control-group">
					    <label class="control-label" for="eventStart">Start Date</label>
					    <div class="controls">
					      <input type="text" id="eventStart" name="eventStart" placeholder="">
					    </div>
					  </div>
					  <div class="control-group">
					    <label class="control-label" for="eventEnd">End Date</label>
					    <div class="controls">
					      <input type="text" id="eventEnd" name="eventEnd" placeholder="">
					    </div>
					  </div>
					  <div class="control-group">
					    <label class="control-label" for="eventTime">Event Time</label>
					    <div class="controls">
					      <input type="text" id="eventTime" name="eventTime" placeholder="Ex 7:00am">
					    </div>
					  </div>
					  <div class="control-group">
					    <label class="control-label" for="eReg">Event Reg URL</label>
					    <div class="controls">
					      <input type="text" id="eReg" name="eReg" placeholder="BikeReg or USAC etc">
					    </div>
					  </div>
					  <!--
					  <div class="control-group">
					    <label class="control-label" for="eventLocation">Event Location</label>
					    <div class="controls">
					      <input type="text" id="eventLocation" name="eventLocation" placeholder="Enter Address">
					    </div>
					    <br/>
					    <label class="control-label" for="eventLocation2">- OR - Trail</label>
					    <div class="controls">
					      <select name="eventTrail" id="eventTrail"><option value="0">-- Choose --</option></select>
					    </div>
					  </div>
-->
					  <div class="control-group">
					    <label class="control-label" for="shortDesc">Short Desc</label>
					    <div class="controls">
					      <textarea id="shortDesc" name="shortDesc" cols="40" rows="4" class="span3"></textarea>
					    </div>
					  </div>
					  <div class="control-group">
					    <label class="control-label" for="homepage">Show On Home Page</label>
					    <div class="controls">
					      <select id="homepage" name="homepage"><option value="0">No</option><option value="1">Yes</option></select>
					    </div>
					  </div>
					 <div class="control-group">
					    <textarea class="jquery_ckeditor" cols="80" id="editor1" name="editor1" rows="10"></textarea>
					  </div>
					</form>
		    	</div>
		    	<div class="modal-footer">
			    	<a href="#" id="saveEvent" class="btn btn-success">Save</a>
		    	</div>
		    </div>
		    <div id="addImg" style="display:none" class="modal hide">
		    	<form id="img_frm" method="POST" enctype="multipart/form-data">
			    	<div class="modal-header">Add Image<button type="button" class="close pull-right" data-dismiss="modal" aria-hidden="true">×</button></div>
			    	<div class="modal-body">
			    		<input type="hidden" value="" id="eid" name="id"/>
			    		<input type="file" name="eventImg" id="eventImg"/>
			    		<div id="img"></div>
			    	</div>
			    	<div class="modal-footer pull-right">
			    		<a href="#" class="btn btn-primary" id="uploadImg" onclick="return addEventFile();">Upload Image</a>
			    		<!-- <input type="submit" value="Add"/> -->
			    	</div>
			    </form>
		    </div>
    </div><!--/row-->

    <hr>
    	
    <footer>
        <p>&copy; DORBA 2012</p>
    </footer>

</div><!--/.fluid-container-->
<div class="modal hide fade" id="eventModal">
    <div class="modal-header">
        <span id="eventName"></span>
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    </div>
    <div class="modal-body" id="eventBody"></div>

</div>

<!-- Le javascript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="js/json2.js"></script>
<script src="assets/js/jquery.js"></script>
<script type='text/javascript' src='jquery/jquery-1.8.0.min.js'></script>
<script type='text/javascript' src='jquery/jquery-ui-1.8.23.custom.min.js'></script>
<script type="text/javascript" src="admin/ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="admin/ckeditor/adapters/jquery.js"></script>
<script src="js/ajaxfileupload.js"></script>
<script src="assets/js/bootstrap-transition.js"></script>
<script src="assets/js/bootstrap-alert.js"></script>
<script src="assets/js/bootstrap-modal.js"></script>
<script src="assets/js/bootstrap-dropdown.js"></script>
<script src="assets/js/bootstrap-scrollspy.js"></script>
<script src="assets/js/bootstrap-tab.js"></script>
<script src="assets/js/bootstrap-tooltip.js"></script>
<script src="assets/js/bootstrap-popover.js"></script>
<script src="assets/js/bootstrap-button.js"></script>
<script src="assets/js/bootstrap-collapse.js"></script>
<script src="assets/js/bootstrap-carousel.js"></script>
<script src="assets/js/bootstrap-typeahead.js"></script>
<script src="js/login.js"></script>
<script src="js/myevents.js"></script>
</body>
</html>
