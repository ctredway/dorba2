<?php include("Connections/conn.php"); ?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>DORBA Frozen Series Results</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Le styles -->
    <link href="site.css" rel="stylesheet">
    <link href="assets/css/bootstrap.css" rel="stylesheet">
    <style>
      body {
        padding-top: 60px; /* 60px to make the container go all the way to the bottom of the topbar */
      }
    </style>
    <link href="assets/css/bootstrap-responsive.css" rel="stylesheet">

    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

<body data-spy="scroll" data-target=".subnav" data-offset="50">

<div class="navbar navbar-fixed-top">
      <div class="navbar-inner">
        <div class="container-fluid">
          <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
            <span class="i-bar"></span>
            <span class="i-bar"></span>
            <span class="i-bar"></span>
          </a>
          <a class="brand" href="#">DORBA Race Results</a>
          <div class="nav-collapse">
            <ul class="nav">
              <li id="cgmnu"><a href="index.php">2011 Fall Series</a></li>
              <li id="revmnu" class="active"><a href="frozen.php">2012 Frozen Series</a></li>
              <li id="revmnu"><a href="fall2012.php">2012 Fall Series</a></li> 
            </ul>
			
          </div><!--/.nav-collapse -->
        </div>
      </div>
    </div>
<div class="container">
<table width="100%" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td align="center" width="750">
		<img src="header2.png" alt="header" />
	</tr>
</table>
<div class="subnav">
  <ul class="nav nav-pills">
    <li><a href="#cat1">6hr Men</a></li>
    <li><a href="#cat2">6hr Women</a></li>
    <li><a href="#cat3">6hr Men SS</a></li>
    <li><a href="#cat4">6hr Women SS</a></li>
    <li><a href="#cat5">6hr Relay</a></li>
    <li><a href="#cat6">6hr Coed Relay</a></li>
    <li><a href="#cat7">4hr Men</a></li>
    <li><a href="#cat8">4hr Women</a></li>
    <li><a href="#cat9">4hr Men SS</a></li>
  </ul>
</div>
<table width="100%" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td align="center" width="750">
<?php 
	$query_catRS = "SELECT typeId,typeValue,displayOrder from types where typeName = 'cat' and active = 1 and series = 2 order by displayOrder";
	$catRS = mysql_query($query_catRS, $conn) or die(mysql_error());
	$row_CatRS = mysql_fetch_assoc($catRS);
	$totalRows_channelRS = mysql_num_rows($catRS);
	
	do{
		if($row_CatRS['typeValue'] == "6 HR SOLO MEN"){
			$cat = "cat1";
		} else if($row_CatRS['typeValue'] == "6 HR SOLO WOMEN"){
			$cat = "cat2";
		} else if($row_CatRS['typeValue'] == "6 HR SOLO SS MEN"){
			$cat = "cat3";
		} else if($row_CatRS['typeValue'] == "6 HR SOLO SS WOMEN"){
			$cat = "cat4";
		} else if($row_CatRS['typeValue'] == "6 HR 2 MAN RELAY"){
			$cat = "cat5";
		} else if($row_CatRS['typeValue'] == "6 HR 2P COED RELAY"){
			$cat = "cat6";
		} else if($row_CatRS['typeValue'] == "4 HR SOLO MEN"){
			$cat = "cat7";
		} else if($row_CatRS['typeValue'] == "4 HR SOLO WOMEN"){
			$cat = "cat8";
		} else if($row_CatRS['typeValue'] == "4 HR SOLO SS MEN"){
			$cat = "cat9";
		} else {
			$cat = "pro";
		}

 ?>
 <div id="<?php echo $cat; ?>">
 <table width="750" border="0" cellpadding="2" cellspacing="1">
 	<tr>
 		<td class="cat"><?php echo $row_CatRS['typeValue']; ?></td>
 	</tr>
 </table>


<table width="750" border="0" cellpadding="1" cellspacing="1">
	
<?php
		//echo $row_RS['racerId']."<br>";
		$query_resultRS = "SELECT racerId,name,team,total from racers inner join tally on racerId = racer where class = ".$row_CatRS['typeId']." order by total desc";
		$resultsRS = mysql_query($query_resultRS, $conn) or die(mysql_error());
		$resultRow_RS = mysql_fetch_assoc($resultsRS);
		$totalRacers = mysql_num_rows($resultsRS);
		
		if($totalRacers !=0){
?>
	<tr>
		<td class="ageGroup" colspan="3"><?php echo $row_RS['typeValue']; ?></td>
	</tr>
	<tr class="header">
		<td>Name</td>
		<td>Team</td>
		<td>&nbsp;</td>
	</tr>
<?php
		$rowClass = 'rowLight';
		do{
			//$place ++;
		?>
			<tr class="<?php echo $rowClass; ?>">
				<td onclick="showResults(<?php echo $resultRow_RS['racerId']; ?>)" style="cursor:pointer"><?php echo $resultRow_RS['name']; ?></td>
				<td><?php echo $resultRow_RS['team']; ?></td>
				<td class="points" align="center"><?php echo $resultRow_RS['total']; ?></td>
			</tr>
		<?php
			if($rowClass == 'rowLight'){
				$rowClass = 'rowDark';
			} else {
				$rowClass = 'rowLight';
			}
			
			$q_racerRes = "select * from results inner join races on raceId = race where racer = ".$resultRow_RS['racerId'];
			$racerResResultS = mysql_query($q_racerRes, $conn) or die(mysql_error());
			$racerResRow_RS = mysql_fetch_assoc($racerResResultS);
			
		?>
			<tr>
				<td colspan="3">
					<div id="<?php echo $racerResRow_RS['racer']; ?>" style="display:none">
					<table width="100%" border="0" cellpadding="1" cellspacing="1">
					
		<?php
			$resultRowClass = 'raceLight';
			$resultRowClass2 = 'raceDark';
			do{
		?>
					<tr class="raceHeader">
						<td>Race</td>
						<td class="<?php echo $resultRowClass; ?>"><?php echo $racerResRow_RS['raceName']; ?></td>
					</tr>
					<tr class="raceHeader">
						<td>Place</td>
						<td class="<?php echo $resultRowClass2; ?>"><?php echo $racerResRow_RS['place']; ?></td>
					</tr>
					<tr class="raceHeader">
						<td>Lap 1 Time</td>
						<td class="<?php echo $resultRowClass; ?>"><?php echo $racerResRow_RS['lap1Time']; ?></td>
					</tr>
					<tr class="raceHeader">
						<td>Lap 1 Pace</td>
						<td class="<?php echo $resultRowClass2; ?>"><?php echo $racerResRow_RS['lap1Pace']; ?></td>
					</tr>
					<tr class="raceHeader">
						<td>Lap 2 Time</td>
						<td class="<?php echo $resultRowClass; ?>"><?php echo $racerResRow_RS['lap2Time']; ?></td>
					</tr>
					<tr class="raceHeader">
						<td>Lap 2 Pace</td>
						<td class="<?php echo $resultRowClass2; ?>"><?php echo $racerResRow_RS['lap2Pace']; ?></td>
					</tr>
					<tr class="raceHeader">
						<td>Lap 3 Time</td>
						<td class="<?php echo $resultRowClass; ?>"><?php echo $racerResRow_RS['lap3Time']; ?></td>
					</tr>
					<tr class="raceHeader">
						<td>Lap 3 Pace</td>
						<td class="<?php echo $resultRowClass2; ?>"><?php echo $racerResRow_RS['lap3Pace']; ?></td>
					</tr>
					<tr class="raceHeader">
						<td>Lap 4 Time</td>
						<td class="<?php echo $resultRowClass; ?>"><?php echo $racerResRow_RS['lap4Time']; ?></td>
					</tr>
					<tr class="raceHeader">
						<td>Lap 4 Pace</td>
						<td class="<?php echo $resultRowClass2; ?>"><?php echo $racerResRow_RS['lap4Pace']; ?></td>
					</tr>
					</tr>
					<tr class="raceHeader">
						<td>Lap 5 Time</td>
						<td class="<?php echo $resultRowClass; ?>"><?php echo $racerResRow_RS['lap5Time']; ?></td>
					</tr>
					<tr class="raceHeader">
						<td>Lap 5 Pace</td>
						<td class="<?php echo $resultRowClass2; ?>"><?php echo $racerResRow_RS['lap5Pace']; ?></td>
					</tr>
					<tr class="raceHeader">
						<td>Lap 6 Time</td>
						<td class="<?php echo $resultRowClass; ?>"><?php echo $racerResRow_RS['lap6Time']; ?></td>
					</tr>
					<tr class="raceHeader">
						<td>Lap 6 Pace</td>
						<td class="<?php echo $resultRowClass2; ?>"><?php echo $racerResRow_RS['lap6Pace']; ?></td>
					</tr>
					<tr class="raceHeader">
						<td>Lap 7 Time</td>
						<td class="<?php echo $resultRowClass; ?>"><?php echo $racerResRow_RS['lap7Time']; ?></td>
					</tr>
					<tr class="raceHeader">
						<td>Lap 7 Pace</td>
						<td class="<?php echo $resultRowClass2; ?>"><?php echo $racerResRow_RS['lap7Pace']; ?></td>
					</tr>
					<tr class="raceHeader">
						<td>Lap 8 Time</td>
						<td class="<?php echo $resultRowClass; ?>"><?php echo $racerResRow_RS['lap8Time']; ?></td>
					</tr>
					<tr class="raceHeader">
						<td>Lap 8 Pace</td>
						<td class="<?php echo $resultRowClass2; ?>"><?php echo $racerResRow_RS['lap8Pace']; ?></td>
					</tr>
					<tr class="raceHeader">
						<td>Lap 9 Time</td>
						<td class="<?php echo $resultRowClass; ?>"><?php echo $racerResRow_RS['lap9Time']; ?></td>
					</tr>
					<tr class="raceHeader">
						<td>Lap 9 Pace</td>
						<td class="<?php echo $resultRowClass2; ?>"><?php echo $racerResRow_RS['lap9Pace']; ?></td>
					</tr>

		<?php 
			if($resultRowClass == 'raceLight'){
				$resultRowClass = 'raceDark';
			} else {
				$resultRowClass = 'raceLight';
			}
			
			} while ($racerResRow_RS = mysql_fetch_assoc($racerResResultS)); ?>
						</table>
					</div>
				</td>
			</tr>
		<?php
			
			} while ($resultRow_RS = mysql_fetch_assoc($resultsRS));
				$place = 0;
			}
		
		?>
</table>
</div>
<?php
		
	} while ($row_CatRS = mysql_fetch_assoc($catRS));  
?>

</td>
	</tr>
</table>
</div>
	<script type="text/javascript" src="http://platform.twitter.com/widgets.js"></script>
    <script src="assets/js/jquery.js"></script>
    <script src="assets/js/bootstrap-transition.js"></script>
    <script src="assets/js/bootstrap-alert.js"></script>
    <script src="assets/js/bootstrap-modal.js"></script>
    <script src="assets/js/bootstrap-dropdown.js"></script>
    <script src="assets/js/bootstrap-scrollspy.js"></script>
    <script src="assets/js/bootstrap-tab.js"></script>
    <script src="assets/js/bootstrap-tooltip.js"></script>
    <script src="assets/js/bootstrap-popover.js"></script>
    <script src="assets/js/bootstrap-button.js"></script>
    <script src="assets/js/bootstrap-collapse.js"></script>
    <script src="assets/js/bootstrap-carousel.js"></script>
    <script src="assets/js/bootstrap-typeahead.js"></script>

<script>
	$(function(){
		// fix sub nav on scroll
	    var $win = $(window)
	      , $nav = $('.subnav')
	      , navTop = $('.subnav').length && $('.subnav').offset().top - 40
	      , isFixed = 0
	
	    processScroll()
	
	    $win.on('scroll', processScroll)
	
	    function processScroll() {
	      var i, scrollTop = $win.scrollTop()
	      if (scrollTop >= navTop && !isFixed) {
	        isFixed = 1
	        $nav.addClass('subnav-fixed')
	      } else if (scrollTop <= navTop && isFixed) {
	        isFixed = 0
	        $nav.removeClass('subnav-fixed')
	      }
	    }
	});
	function hideShow(what){
		//alert(what);
		switch(what){
			case "cat1":
				$("#cat1").show();
				$("#cat2").hide();
				$("#cat3").hide();
				$("#pro").hide();
				break;
			case "cat2":
				$("#cat1").hide();
				$("#cat2").show();
				$("#cat3").hide();
				$("#pro").hide();
				break;
			case "cat3":
				$("#cat1").hide();
				$("#cat2").hide();
				$("#cat3").show();
				$("#pro").hide();
				break;
			case "pro":
				$("#cat1").hide();
				$("#cat2").hide();
				$("#cat3").hide();
				$("#pro").show();
				break;
			default:
				$("#cat1").show();
				$("#cat2").show();
				$("#cat3").show();
				$("#pro").show();
				break;
		}
		
	}
	
	function showResults(what){
		$("#"+what).toggle();
	}
</script>
</body>
</html>