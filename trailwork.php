<?php include("header.php") ?>
<?php 
	//include("Connections/conn.php"); 
	
	$query_catRS = "CALL GetTrails(0);";
	$catRS = mysql_query($query_catRS, $conn) or die(mysql_error());
	
	$row_CatRS = mysql_fetch_assoc($catRS);
	
	//$totalRows_channelRS = mysql_num_rows($catRS);
	
?>
    <div class="container-fluid">
      <div class="row-fluid">
        
        <div class="span12">
        	<ul class="breadcrumb">
			  <li>
			    <a href="/">Home</a> <span class="divider">/</span>
			  </li>
			  <li class="active">
			    <a >My Trail Work</a> 
			  </li>
			  
			</ul>
          <div class="row-fluid" id="content">
            <div id="choose" class="well well-small span5">
				<p>Enter your trail work here:</p>
				<form id="work_frm" class="form-inline">
					<div class="control-group">
						<label class="control-label" for="memberType">Trail:</label>
						<div class="controls">
							<select id="trail" name="trail">
                                <option value="0">-- Choose --</option>
                                <option value="Other">Other</option>
								<?php do { ?> 
					              <option value="<?php echo $row_CatRS['trailName']; ?>"><?php echo $row_CatRS['trailName']; ?></option>
					              <?php } while ($row_CatRS = mysql_fetch_assoc($catRS)); ?> 
					              
							</select>
						</div>
					</div>
					<div class="control-group">
						<label class="control-label" for="hours">Hours Worked:</label>
						<div class="controls">
							<input type="text" id="hours" name="hours"/>
						</div>
					</div>
					<div class="control-group">
						<label class="control-label" for="hours">Date Worked:</label>
						<div class="controls">
							<input type="text" id="workDate" name="workDate"/>
						</div>
					</div>
					<div class="control-group">
						<div class="controls">
						<textarea id="work" name="work" cols="40" rows="6" class="span9"></textarea>
						</div>
					</div>
					<div class="control-group">
						<div>
							<a id="newWork" class="btn btn-primary pull-left">New</a>
							<a id="saveWork" class="btn btn-success pull-right">Save Work</a>
						</div>
					</div>
					<input type="hidden" name="memberId" id="memberId"/>
					<input type="hidden" name="workId" id="workId"/>
				</form>
			</div>
			<div class="span7" id="workArea">
				<table class="table table-striped table-bordered table-condensed" id="trailList_tbl">
					<thead>
					  <tr>
					    <th></th>
					    <th>Trail</th>
					    <th>Comments</th>
					    <th>Hours</th>
					  </tr>
					</thead>
					<tbody id="trailWorkBody"></tbody>
					
				</table>
			</div>
			<div id="loggedOut" class="alert alert-danger" style="display:none;">
				Please login to log trail hours
			</div>
          </div>
        </div><!--/span-->
        <!--
<div class="span2 offest1">
          <!--
<div class="well sidebar-nav">
            <ul class="nav nav-list">
              <li class="nav-header">Trail Listing</li>
              <?php 
              	$open = "class='open'";
              	$closed = "class='closed'";
              	$status='';
              do { 
	              if($list['currentStatus'] == 1){ $statusClass=$open; $status = "Open";} else { $statusClass=$closed; $status="Closed";};
	              $content = "Conditions: ".$list['conditionDesc']."<br/>Status: ".$status;
	              
              ?> 
              <li><a <?php echo $statusClass; ?> href="trail.php?t=<?php echo $list['trailId']; ?>" rel="popover" data-content="<?php echo $content; ?>" data-original-title="<?php echo $list['trailName']; ?>"><?php echo $list['trailName']; ?></a></li>
              <?php } while ($list = mysql_fetch_assoc($catRS2)); ?>       
            </ul>

           
<ul class="nav nav-list" id="sponsorList">
              <li class="nav-header">DORBA Sponsors</li>
                   
            </ul>

          </div>
          
        </div>
-->
        
      </div><!--/row-->

      <hr>

      <footer>
        <p>&copy; DORBA 2012</p>
      </footer>

    </div><!--/.fluid-container-->
    
    <!-- Le javascript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="js/json2.js"></script>
    <script src="assets/js/jquery.js"></script>
    <script type="text/javascript" src="jquery/jquery-ui-1.8.23.custom.min.js"></script>
    <script src="assets/js/bootstrap-transition.js"></script>
    <script src="assets/js/bootstrap-alert.js"></script>
    <script src="assets/js/bootstrap-modal.js"></script>
    <script src="assets/js/bootstrap-dropdown.js"></script>
    <script src="assets/js/bootstrap-scrollspy.js"></script>
    <script src="assets/js/bootstrap-tab.js"></script>
    <script src="assets/js/bootstrap-tooltip.js"></script>
    <script src="assets/js/bootstrap-popover.js"></script>
    <script src="assets/js/bootstrap-button.js"></script>
    <script src="assets/js/bootstrap-collapse.js"></script>
    <script src="assets/js/bootstrap-carousel.js"></script>
    <script src="assets/js/bootstrap-typeahead.js"></script>
    <script src="js/login.js"></script>
    <script src="js/trailwork.js"></script>
  </body>
</html>
