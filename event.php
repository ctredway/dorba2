 <?php include("header.php") ?>
<?php 
	
//include("Connections/conn.php"); 
	
	if(isset($_REQUEST['e'])){
		$query_catRS = "CALL GetEvents(".$_REQUEST['e'].");";
		$catRS = mysql_query($query_catRS, $conn) or die(mysql_error());
		$row_CatRS = mysql_fetch_assoc($catRS);
		$totalRows_channelRS = mysql_num_rows($catRS);

		if($totalRows_channelRS == 0){
			header( 'Location: events.php' ) ;
		}
	} else {
		header( 'Location: events.php' ) ;
	}
	
?>
    <div class="container-fluid">

      <div class="row-fluid">
          <div class="span3 content_buffer">
              <div class="well ">
                  <h4><?php echo $row_CatRS['eventStart']; ?></h4>
                  <?php
                  if($row_CatRS['regUrl'] != ''){
                      ?>
                      <a href="<?php echo $row_CatRS['regUrl']; ?>">REGISTER</a>
                  <?php
                  }
                  ?>
                  <div id="rsvp" style="display:none"><span id="msg"><strong>Are you going?</strong></span> <a href="#" id="goto" class="btn btn-success btn-mini" onclick="rsvp(<?php echo $row_CatRS['eventId']; ?>,1)"><i class="icon-thumbs-up"></i></a> <a href="#" id="nogo" class="btn btn-danger btn-mini" onclick="rsvp(<?php echo $row_CatRS['eventId']; ?>,0)"><i class="icon-thumbs-down"></i></a></div>
                  <?php if($row_CatRS['eventFlyer'] != '' || $row_CatRS['eventFlyer'] != null){ ?>
                      <img src="<?php echo $row_CatRS['eventFlyer']; ?>" class="img-polaroid"/>
                  <?php
                  }
                  ?>
              </div>
          </div><!--/span-->
        <div class="span9" style="padding-top: 30px;">

	         <div class="well">
                  <h4><?php echo $row_CatRS['eventName']; ?></h4>
	           <P> <?php echo $row_CatRS['eventDesc']; ?></P>
	          </div>

        </div><!--/span-->
      </div><!--/row-->


      <footer>
        <p>&copy; DORBA 2012</p>
      </footer>

    </div><!--/.fluid-container-->
    
    <!-- Le javascript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="js/json2.js"></script>
    <script src="assets/js/jquery.js"></script>
    <script src="assets/js/bootstrap-transition.js"></script>
    <script src="assets/js/bootstrap-alert.js"></script>
    <script src="assets/js/bootstrap-modal.js"></script>
    <script src="assets/js/bootstrap-dropdown.js"></script>
    <script src="assets/js/bootstrap-scrollspy.js"></script>
    <script src="assets/js/bootstrap-tab.js"></script>
    <script src="assets/js/bootstrap-tooltip.js"></script>
    <script src="assets/js/bootstrap-popover.js"></script>
    <script src="assets/js/bootstrap-button.js"></script>
    <script src="assets/js/bootstrap-collapse.js"></script>
    <script src="assets/js/bootstrap-carousel.js"></script>
    <script src="assets/js/bootstrap-typeahead.js"></script>
    <!-- <script src="js/app.js"></script> -->
     <script src="js/login.js"></script> 
    <script src="js/event.js"></script>
  </body>
</html>
