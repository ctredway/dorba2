
    <?php include("header.php") ?>

    <div class="container" style="padding-top: 20px;">

      <div class="row-fluid">
          <div class="span3 content_buffer">
              <div class="well well-small">
                   <h4 class="text-center">UPCOMING EVENTS</h4>
                  <ul class="nav nav-list" id="eventArea"></ul>
              </div>
              <div class="well well-small">
                  <h4 class="text-center">MAP CONTROLS</h4>
                  <div>
                      <h5>SHOW ONLY</h5>
                      <a href="#" onclick="showOpenClosed(1);" class="open1">OPEN</a> | <a href="#" onclick="showOpenClosed(2);" class="closed1">CLOSED</a>
                      <ul>
                        <li><a href="#" onclick="showOpenClosed(3);">ALL TRAILS</a></li>
                        <li><a href="#">DORBA TRAILS</a></li>
                        <li><a href="#">SPONSORED TRAILS</a></li>
                        <li><a href="#">REGIONAL TRAILS</a></li>
                      </ul>
                  </div>
              </div>
          </div><!--/span-->
        <div class="well well-small span9">

        	<h2 class="text-center">The Trails</h2>

        	<div class="row-fluid hidden-phone">

	          <div class="well" id="cgmap">
	            
	          </div>
        	</div>
          <div class="row-fluid">
          
          <div id="openSpan">
          <table class="table table-striped table-condensed" id="trailList_tbl">
			<thead>
			  <tr>
			    <th><a class="btn btn-info btn-mini" id="viewAllTrails">View All</a></th>
			    <th>Name</th>
			    <th>Location</th>
			    <th>Condition</th>
			  </tr>
			</thead>
			<tbody id="openlistBody"></tbody>
		</table>
			<div id="edit_frm_div" class="modal hide">
				
				<div class="modal-header"><strong>Update Trail</strong>
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
				</div>
				<div class="modal-body">
					<form id="frm_edit_trail" class="form-horizontal">
						<input type="hidden" name="t" id="t"/>
						<input type="hidden" name="u" id="u"/>
						<div class="control-group">
							<label class="control-label" for="trailStatus">Trail Status:</label>
							<div class="controls">
								<select name="trailStatus"><option value="0">Closed</option><option value="1">Open</option></select>
							</div>
						</div>
						<div class="control-group">
							<label class="control-label" for="condition">Trail Condition:</label>
							<div class="controls">
								<select name="condition" id="condition"></select>
							</div>
						</div>
						<div class="control-group">
							
							<div class="controls">
								<textarea name="trailComment" cols="40" rows="3"></textarea>  <span class="help-inline">Leave a comment</span>
							</div>
						</div>
						
					</form>
				</div>
				<div class="modal-footer">
					<a id="cancel_edit" data-dismiss="modal" class="btn btn-danger">Cancel</a> <a class="btn btn-success pull-right" onclick="save_edit_frm()">Submit</a>
				</div>
			</div>
          </div>
<!--            <div class="span6" style="display:none" id="closedTrail">-->
<!--           <h4>Closed</h4>-->
<!--	            <table class="table table-striped table-bordered table-condensed" id="trailList_tbl">-->
<!--			<thead>-->
<!--			  <tr>-->
<!--			    <th><a class="btn btn-info btn-mini" id="viewAllTrails2">View All</a></th>-->
<!--			    <th>Name</th>-->
<!--			    <th>Location</th>-->
<!--			    <th>Condition</th>-->
<!--			  </tr>-->
<!--			</thead>-->
<!--			<tbody id="closedlistBody"></tbody>-->
<!--		</table>-->
<!--            </div>-->
          </div><!--/row-->
        </div><!--/span-->
        

      </div><!--/row-->
        <div class="span2 offset4"><a href="http://mountainbiketx.com/" target="_blank"> Visit Mountain Bike Texas </a></div>
      <hr>

      <footer>
        <p>&copy; DORBA 2012</p>
      </footer>

    </div><!--/.fluid-container-->
    
    <?php 
		include('trailedit.html');
    ?>
    
    <!-- Le javascript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="js/json2.js"></script>
    <script src="assets/js/jquery.js"></script>
    <script type="text/javascript" src="admin/ckeditor/ckeditor.js"></script>
    <script type="text/javascript" src="admin/ckeditor/adapters/jquery.js"></script>
    <script src="assets/js/bootstrap-transition.js"></script>
    <script src="assets/js/bootstrap-alert.js"></script>
    <script src="assets/js/bootstrap-modal.js"></script>
    <script src="assets/js/bootstrap-dropdown.js"></script>
    <script src="assets/js/bootstrap-scrollspy.js"></script>
    <script src="assets/js/bootstrap-tab.js"></script>
    <script src="assets/js/bootstrap-tooltip.js"></script>
    <script src="assets/js/bootstrap-popover.js"></script>
    <script src="assets/js/bootstrap-button.js"></script>
  
    <script src="assets/js/bootstrap-collapse.js"></script>
  <!--    <script src="assets/js/bootstrap-carousel.js"></script>
    <script src="assets/js/bootstrap-typeahead.js"></script>
-->
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false"></script>
    <!-- <script src="js/app.js"></script> -->
    <script src="js/login.js"></script>
    <script src="js/trails.js"></script>
    
    <script>
    $(function(){
	   $("#home").removeClass("active");
	   $("#trails").addClass("active"); 
	   $("#forums").removeClass("active");
	   $("#events").removeClass("active");
	   $("#sponsors").removeClass("active");
    });
    </script>
  </body>
</html>
