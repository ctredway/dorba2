<?php
include("session.php");
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>DORBA Admin</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
	<META HTTP-EQUIV="Pragma" CONTENT="no-cache">
	<META HTTP-EQUIV="Expires" CONTENT="-1">

    <!-- Le styles -->
    <link href="../assets/css/bootstrap.css" rel="stylesheet">
    <style>
      body {
        padding-top: 60px; /* 60px to make the container go all the way to the bottom of the topbar */
      }
    </style>
    <link href="../assets/css/bootstrap-responsive.css" rel="stylesheet">
    <link type="text/css" href="../assets/css/ui-lightness/jqui.css" rel="stylesheet" />
    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <!-- Le fav and touch icons -->
    <link rel="shortcut icon" href="../assets/ico/favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="../assets/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="../assets/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="../assets/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="../assets/ico/apple-touch-icon-57-precomposed.png">
  </head>

  <body>

  <?php include('navbar.html'); ?>

    <div class="container-fluid">
    	<div class="row">
	    <div id="main_container" class="row-fluid">
            <?php include('navmenu.html'); ?>
			<div class="span9">
				<div>
					<form id="search_frm" class="form-inline"><label for="searchtxt">Search for Member: </label><input type="search" id="searchtxt" class="input-large search-query"> <a id="search_btn" class="btn btn-success btn-small">Search</a> 
					<!--
<label class="checkbox">
						<input type="checkbox" id="memberOnly"> Show Members only
						</label> 
-->
					</form>
					
				</div>
                <ul class="nav nav-tabs" id="memberTabs">
                    <li><a href="#current" data-toggle="tab">Current</a></li>
                    <li><a href="#expired" data-toggle="tab">Expired</a></li>
                    <li><a href="#forum" data-toggle="tab">Forum</a></li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane" id="current">
                        <table class="table table-bordered table-hover table-condensed table-striped" id="memberTbl">
                          <thead>
                            <tr>
                              <th><a class="btn btn-primary btn-mini" id="addmem">Add</a></th>
                              <th>Name</th>
                              <!-- <th>User Name</th> -->
                              <th>Email</th>
                              <th>Membership Ends</th>
                              <th></th>
                            </tr>
                          </thead>
                          <tbody id="memberList">
                          </tbody>
                        </table>

                    </div>
                    <div id="expired" class="tab-pane">
                        <div class="well well-small">
                            Start Date:<input type="text" class="input-medium" id="sdate"> End Date:<input type="text" class="input-medium" id="edate"><a href="#" class="btn btn-info btn-small" id="run">Run</a> <a href="file.csv" id="download" style="display: none"><i class="icon-download icon-white"></i> CSV</a>
                        </div>
                        <table class="table table-bordered table-hover table-condensed table-striped" id="expTbl">
                            <thead>
                            <tr>
                                <th><a class="btn btn-primary btn-mini" id="addmem">Add</a></th>
                                <th>User Name</th>
                                <!-- <th>User Name</th> -->
                                <th>Email</th>
                                <th></th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody id="expiredList">
                            </tbody>
                        </table>
                    </div>
                    <div id="forum" class="tab-pane">
                        <table class="table table-bordered table-hover table-condensed table-striped" id="forumTbl">
                            <thead>
                            <tr>
                                <th><a class="btn btn-primary btn-mini" id="addmem">Add</a></th>
                                <th>User Name</th>
                                <!-- <th>User Name</th> -->
                                <th>Email</th>
                                <th></th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody id="forumList">
                            </tbody>
                        </table>
                    </div>
			    </div>
                <?php include('newmember.php'); ?>
                <div id="confirm" class="modal hide fade">
                    <input type="hidden" id="mem">
                    <div class="modal-header">Please Confirm!</div>
                    <div class="modal-body">
                        Are you sure you want to delete this record?
                    </div>
                    <div class="modal-footer">
                        <div class="pull-right">
                            <a id="deleteCancel" href="#" class="btn btn-danger" data-dismiss="modal">Cancel</a>
                            <a id="deleteYes" href="#" class="btn btn-success">Yes, Delete</a>
                        </div>
                    </div>
                </div>
            </div>
	    </div>
      
    </div> <!-- /container -->
    <footer class="footer offset2"></footer>
    
    <!-- Le javascript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="../assets/js/jquery.js"></script>
    <script type="text/javascript" src="../jquery/jquery-ui-1.8.23.custom.min.js"></script>
    <script src="../assets/js/bootstrap.min.js"></script>
    <script src="js/members.js"></script>
  </body>
</html>
