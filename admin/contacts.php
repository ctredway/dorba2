<?php
include("session.php");
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>DORBA Admin</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <META HTTP-EQUIV="Pragma" CONTENT="no-cache">
    <META HTTP-EQUIV="Expires" CONTENT="-1">
    <META HTTP-EQUIV="Pragma" CONTENT="no-cache">
    <META HTTP-EQUIV="Expires" CONTENT="-1">

    <!-- Le styles -->
    <link href="../assets/css/bootstrap.css" rel="stylesheet">
    <style>
        body {
            padding-top: 60px; /* 60px to make the container go all the way to the bottom of the topbar */
        }
    </style>
    <link href="../assets/css/bootstrap-responsive.css" rel="stylesheet">

    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <!-- Le fav and touch icons -->
    <link rel="shortcut icon" href="../assets/ico/favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="../assets/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="../assets/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="../assets/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="../assets/ico/apple-touch-icon-57-precomposed.png">
</head>

<body>
<?php
    include('../Connections/conn.php');

    $query_catRS = "Select bodContactId,firstName,lastName,email,bodTitle from bodContacts";
    $result = $mysqli->query( $query_catRS);


?>
<?php include('navbar.html'); ?>

<div class="container">
    <div class="row">
        <div id="main_container" class="row-fluid">
            <?php include('navmenu.html'); ?>
            <div class="span9">
                <div id="main">
                <h4>Website Contacts</h4>
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th><a href="#" class="btn btn-small btn-success" id="newContact_btn"><i class="icon-plus-sign icon-white"></i></a> </th>
                            <th>Name</th>
                            <th>Title</th>
                            <th>Email</th>
                        </tr>
                    </thead>
                    <tbody id="contact_list">
                    <?php while ($row = $result->fetch_assoc()) { ?>

                        <tr><td><a href="#" class="btn btn-small btn-success"><i class="icon-wrench"></i></a> <a href="#" id="<?php echo $row['bodContactId']; ?>" class="btn btn-small btn-danger deleteContact"><i class="icon-remove-sign"></i></a> </td><td><?php echo $row['firstName']." ".$row['lastName']; ?></td><td><?php echo $row['bodTitle']; ?></td><td><?php echo $row['email']; ?></td></tr>
                       <?php }
                            $result->close();
                        ?>
                    </tbody>
                </table>
                </div>
                <div id="contact" class="span6" style="display: none">
                    <form id="contact_form">
                        <input type="hidden" name="cid" id="cid" value="0">
                        <div class="control-group">
                            <label class="control-label" for="fname">First Name</label>
                            <div class="controls">
                                <input type="text" id="fname" name="fname" placeholder="First Name">
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label" for="lname">Last Name</label>
                            <div class="controls">
                                <input type="text" id="lname" name="lname" placeholder="Last Name">
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label" for="email">Email</label>
                            <div class="controls">
                                <input type="text" id="email" name="email" placeholder="Email">
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label" for="title">Title</label>
                            <div class="controls">
                                <input type="text" id="title" name="title" placeholder="Vp, President, Treasurer, etc">
                            </div>
                        </div>
                        <div class="control-group">
                            <div class="controls">
                                <a href="#" class="btn btn-small btn-danger" id="cancelContact">Cancel</a>
                                <a href="#" class="btn btn-small btn-success" id="saveContact">Save</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>

        </div>

    </div> <!-- /container -->
    <footer class="footer offset2">Woot</footer>
    <!-- Le javascript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="../assets/js/jquery.js"></script>
    <script src="../assets/js/bootstrap.js"></script>
    <script src="../js/admin/contacts.js"></script>
</body>
</html>
