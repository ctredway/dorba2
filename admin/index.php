<?php
    session_start();
    //var_dump($_SESSION);
    if(isset($_SESSION['id']) && $_SESSION['id'] != 0){
        //var_dump($_SESSION);
        header("Location: main.php");
    }
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>DORBA Admin</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
	<META HTTP-EQUIV="Pragma" CONTENT="no-cache">
	<META HTTP-EQUIV="Expires" CONTENT="-1">
    <!-- Le styles -->
    <link href="../assets/css/bootstrap.css" rel="stylesheet">
    <style>
      body {
        padding-top: 60px; /* 60px to make the container go all the way to the bottom of the topbar */
      }
    </style>
    <link href="../assets/css/bootstrap-responsive.css" rel="stylesheet">

    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <!-- Le fav and touch icons -->
    <link rel="shortcut icon" href="../assets/ico/favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="../assets/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="../assets/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="../assets/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="../assets/ico/apple-touch-icon-57-precomposed.png">
  </head>

  <body>

    <div class="navbar navbar-inverse navbar-fixed-top">
      <div class="navbar-inner">
        <div class="container">
          <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </a>
          <a class="brand" href="#">DORBA Admin</a>
          <!--/.nav-collapse -->
        </div>
      </div>
    </div>

    <div class="container">
    	<div class="row">
	    <div id="main_container" class="row-fluid">
	  		<?php
                if(isset($_REQUEST['e']) && $_REQUEST['e'] == 1){
            ?>
             <div id="loginError" class="alert alert-danger">You do not have access to this part of the site.</div>
            <?php
                }
            ?>
			<div class="span4">
				<form class="form-horizontal" action="login.php" method="post">
					<div class="control-group" id="email_grp">
					    <label class="control-label" for="u">User Name</label>
					    <div class="controls">
					      <input type="text" id="u" name="u" placeholder="Enter Your User name">
					    </div>
					  </div>
					  <div class="control-group" id="subject_grp">
					    <label class="control-label" for="p">Password</label>
					    <div class="controls">
					      <input type="password" id="p" name="p" placeholder="Enter Your Password">
					    </div>
					  </div>
					  <div class="control-group">
					  	 <input type="submit" value="Login" class="btn btn-primary pull-right">
					  </div>
				</form>
			</div>
            <?php if(isset($_REQUEST[e]) && $_REQUEST['e'] != 1){ ?>
             <div class="span4 alert-danger">Your login was incorrect, please try again.</div>
            <?php } ?>
	    </div>
      
    </div> <!-- /container -->
    <footer class="footer offset2"></footer>
    <!-- Le javascript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="../assets/js/jquery.js"></script>
    <script src="../assets/js/bootstrap.js"></script>

    <script src="js/app.js"></script>

  </body>
</html>
