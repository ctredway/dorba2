<?php
include("session.php");
?>
<!DOCTYPE html>
 <html>
  <head>
    <meta charset="utf-8">
    <title>DORBA Admin</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    	<META HTTP-EQUIV="Pragma" CONTENT="no-cache">
	<META HTTP-EQUIV="Expires" CONTENT="-1">

     <link href="../assets/css/bootstrap.css" rel="stylesheet">
    <link type="text/css" href="../assets/css/ui-lightness/jqui.css" rel="stylesheet" />
    <style>
      body {
        padding-top: 60px; /* 60px to make the container go all the way to the bottom of the topbar */
      }
    </style>
    <link href="../assets/css/bootstrap-responsive.css" rel="stylesheet">

    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <!-- Le fav and touch icons -->
    <link rel="shortcut icon" href="../assets/ico/favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="../assets/ico/bootstrap-apple-114x114.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="../assets/ico/bootstrap-apple-72x72.png">
    <link rel="apple-touch-icon-precomposed" href="../assets/ico/bootstrap-apple-57x57.png">
  </head>

  <body>

  <?php include('navbar.html'); ?>

    <div class="container-fluid">
    	<div class="row">
	    	<div id="main_container" class="row-fluid">
                <?php include('navmenu.html'); ?>
			<div class="span8" id="middle">
			
				<table class="table table-striped table-bordered table-condensed" id="sponsorList_tbl">
					<thead>
					  <tr>
					    <th><a id="addSponsor" class="btn btn-success btn-mini">Add Sponsor</a></th>
					    <th>Name</th>
					    <th>Website</th>
					    <!-- <th>Location</th> -->
					  </tr>
					</thead>
					<tbody id="sponsorlistBody">
						
					</tbody>
				</table>
				<div id="sponsorForm" style="display:none;">
			    	<div class="modal-header" id="hdr"><h3>New Sponsor</h3></div>
			    	<div class="modal-body">
			    		<form class="form-horizontal" id="sponsor_frm">
			    			<input type="hidden" name="sid" id="sid" value="0"/>
						  <div class="control-group">
						    <label class="control-label" for="sponsorName">Sponsor Name</label>
						    <div class="controls">
						      <input type="text" id="sponsorName" name="sponsorName" placeholder="">
						    </div>
						  </div>
						  <div class="control-group">
						    <label class="control-label" for="website">Website</label>
						    <div class="controls">
						      <input type="text" id="website" name="website" placeholder="">
						    </div>
						  </div>
						  <div class="control-group">
						    <label class="control-label" for="contact">Contact</label>
						    <div class="controls">
						      <input type="text" id="contact" name="contact" placeholder="">
						    </div>
						  </div>
						 <div class="control-group">
						    <textarea class="jquery_ckeditor" cols="80" id="editor1" name="editor1" rows="10"></textarea>
						  </div>
						</form>
			    	</div>
			    	<div class="modal-footer">
			    		<a href="#" class="btn btn-danger btn-mini" id="cancelSponsor">Cancel</a>
				    	<a href="#" id="saveSponsorBtn" class="btn btn-success">Save</a>
			    	</div>
			  </div>
			  <div id="sponsorMgr" style="display:none" class="span6">
			  		<div class="modal-header" id="hdr2"><h3>Sponsor Manager</h3></div>
			    	<div class="modal-body">
			    		<form class="form-horizontal" id="sponsor_admin_frm">
			    			<input type="hidden" name="sid" id="spid" value="0"/>
                            <input type="hidden" name="uid" id="uid" value="0"/>
						  <div class="control-group">
						    <label class="control-label" for="sponsor">User:</label>
						    <div class="controls">
						      <input type="text" id="sponsor" name="sponsor" class="input-medium" placeholder="">
						    </div>
						  </div>
						   
						</form>
                        <table class="table table-bordered" id="sponsor_tbl" style="display: none">
                            <thead>
                            <th></th>
                            <th>User</th>
                            <th>Email</th>
                            </thead>
                            <tbody id="sponsorAdmins">

                            </tbody>
                        </table>
			    	</div>
			    	<div class="modal-footer">
			    		<a href="#" class="btn btn-danger btn-small" id="cancelMgr">Cancel</a>
				    	<a href="#" id="saveMgr" class="btn btn-success btn-small">Save</a>
			    	</div>
			  </div>
			</div>
			<div class="span4">
			
			</div>
	    </div>
	    
    </div> <!-- /container -->
    <footer class="footer offset1">Woot</footer>
    
    
    <!-- Le javascript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script type="text/javascript" src="../assets/js/jquery.js"></script>
	<script type="text/javascript" src="../jquery/jquery-ui-1.8.23.custom.min.js"></script>
    <script type="text/javascript" src="ckeditor/ckeditor.js"></script>
    <script type="text/javascript" src="ckeditor/adapters/jquery.js"></script>
    <script src="../assets/js/bootstrap.js"></script>
    <script src="../js/admin/sponsors.js"></script>

  </body>
</html>
