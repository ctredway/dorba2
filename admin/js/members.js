$(function(){

	getMembers();
    getForumUsers();
   // getExpiredUsers();
	
	$("#addmem").click(function(){
		$("#member").val(0);
		$("#firstName").val('');
		$("#lastName").val('');
		$("#email").val('');
		$("#memberType").val('');
		$("#homePhone").val('');
		$("#cellPhone").val('');
		$("#emerContact").val('');
		$("#endsOn").val('');
		$("#new_member").show();
		$("#memberTbl").hide();
		buildDatePickers();
	});
	
	$("#closeMemFrm").click(closeForm);
	
	$("#cancelMember").click(closeForm);
	
	$("#saveMember").click(function(){
		$.post('../services/updatememberdata.php',$("#member_frm").serialize(),function(data){
			//console.log(data);
			$("#new_member").hide();
			$("#memberTbl").show();
            $("#expTbl").show();
            $("#forumTbl").show();
			getMembers();
		});
	});
	
	$("#search_btn").click(function(){
		$.post('../services/findmember.php',$("#search_frm").serialize(),function(data){
			console.log(data);
			
		})
	});

    $("#deleteYes").click(deleteMember);

    $("#sdate").datepicker();
    $("#edate").datepicker();

    $("#sdate").datepicker("option", "dateFormat", "yy-mm-dd" );
    $("#edate").datepicker("option", "dateFormat", "yy-mm-dd" );

    $("#run").click(getExpiredUsers);
});

function closeForm(){
    $("#new_member").hide();
    $("#memberTbl").show();
    $("#expTbl").show();
    $("#forumTbl").show();
}

function getMembers(){
	$("#memberList").empty();
	$.getJSON('../services/getusers.php',function(data){
		$.each(data.members, function(i,item){
            var mem = '';
            if(item.member.memberType == 1){
                mem = 'Individual';
            } else if(item.member.memberType == 2){
                mem = 'Family';
            } else if(item.member.memberType == 3){
                mem = 'Junior';
            } else {
                mem = 'Corp';
            }
			$("#memberList").append('<tr><td><a href="#" class="btn btn-success btn-mini" onclick="editMember('+item.member.memberDataId+',1);"><i class="icon-edit"></i></a><a href="#" class="btn btn-danger btn-mini" onclick="confirmDelete('+item.member.memberDataId+');"><i class="icon-remove"></i></a></td><td>'+item.member.firstName+' '+item.member.lastName+'</td><td>'+item.member.email+'</td><td>'+item.member.membershipEnds+'</td><td>'+mem+'</td></tr>');
		});
        $( '#memberTabs a:first' ).tab( 'show' );
	});
}

function getForumUsers(){
    $("#forumList").empty();
    $.getJSON('../services/getforumusers.php',function(data){
        $.each(data.members, function(i,item){

            $("#forumList").append('<tr><td><a href="#" class="btn btn-success btn-mini" onclick="convertToMember('+item.member.user_id+');"><i class="icon-edit"></i></a></td><td>'+item.member.username+'</td><td>'+item.member.user_email+'</td><td></td><td></td></tr>');
        });
       // $( '#memberTabs a:first' ).tab( 'show' );
    });
}

function getExpiredUsers(){
    $("#expiredList").empty();
    $("#download").hide();
    var s = $("#sdate").val();
    var e = $("#edate").val();
    $.getJSON('../services/getexpiredmembers.php?s='+s+'&e='+e,function(data){
        $.each(data.members, function(i,item){
            var mem = '';
            if(item.member.memberType == 1){
                mem = 'Individual';
            } else if(item.member.memberType == 2){
                mem = 'Family';
            } else if(item.member.memberType == 3){
                mem = 'Junior';
            } else {
                mem = 'Corp';
            }
            $("#expiredList").append('<tr><td><a href="#" class="btn btn-success btn-mini" onclick="editMember('+item.member.memberDataId+',2);"><i class="icon-edit"></i></a></td><td>'+item.member.firstName+' '+item.member.lastName+'</td><td>'+item.member.email+'</td><td>'+item.member.membershipEnds+'</td><td>'+mem+'</td></tr>');
            $("#download").show();
        });
        // $( '#memberTabs a:first' ).tab( 'show' );
    });
}

function editMember(mem,what){

    var url = '../services/getusers.php?m=';
    if(what == 2){
        url = '../services/getmemberdata.php?m='
    } else if(what == 3){
        $("#member").val(mem);
        $("#firstName").val('');
        $("#lastName").val('');
        $("#email").val('');
        $("#memberType").val('');
        $("#homePhone").val('');
        $("#cellPhone").val('');
        $("#emerContact").val('');
        $("#endsOn").val('');
        $("#new_member").show();
        $("#expTbl").hide();
        $("#forumTbl").hide();
        $("#memberTbl").hide();
        buildDatePickers();
        return;
    }

	$.getJSON(url+mem,function(data){
        console.log(data.members.length);

        $.each(data.members, function(i,item){
            $("#member").val(mem);
            $("#firstName").val(item.member.firstName);
            $("#lastName").val(item.member.lastName);
            $("#email").val(item.member.email);
            $("#memberType").val(item.member.memberType);
            $("#homePhone").val(item.member.homePhone);
            $("#cellPhone").val(item.member.cellPhone);
            $("#emerContact").val(item.member.emergencyContact);
            $("#endsOn").val(item.member.membershipEnds);
            $("#new_member").show();
            $("#expTbl").hide();
            $("#forumTbl").hide();
            $("#memberTbl").hide();
            buildDatePickers();
        });

	});
}

function convertToMember(mem){
    $.get('../services/converttomember.php?m='+mem,function(data){
        editMember(data,3);
    });
}

function buildDatePickers(){
	$( "#endsOn" ).datepicker({
		changeYear:true,onSelect: function(data,instance){$( "#endsOn" ).datepicker( "option", "dateFormat", 'yy-mm-dd' );}});
	
}

function checkUsername(){
	//console.log('checking');
	userName = $("#forumName").val();
	$.get('../services/checkusername.php?u='+userName,function(data){
		//console.log(data);
		if(data == 1){
			$("#forum_cntrl").addClass('error');
			$("#forum_cntrl").removeClass('success');
			alert("that name is taken");
		} else if(data == 0){
			$("#forum_cntrl").addClass('success');
			$("#forum_cntrl").removeClass('error');
		}
	});
}

function confirmDelete(mem){
    $("#confirm").modal();
    $("#confirm").modal('show');
    $("#mem").val(mem);
}

function deleteMember(){
    var mem =  $("#mem").val();
    $.get('../services/deletemember.php?c='+mem,function(data){
        $("#confirm").modal('hide');
       getMembers();
    });
}

function makeMember(){
	
}

function searchUsers(){
	
}