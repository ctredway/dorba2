<?php
    session_start();
   // var_dump($_SESSION);
    if(!isset($_SESSION['id'])){
        header("Location: logout.php");
        /* Make sure that code below does not get executed when we redirect. */
        //exit;
    } else {
        include('../Connections/conn.php');

        $groupCheck = 'Call CheckUserGroups('.$_SESSION['id'].')';
        $runCheck = $mysqli->query($groupCheck);

        $isAdmin = $runCheck->num_rows;

        if($isAdmin == 0){
            header("Location: index.php?e=1");
        }

    }
