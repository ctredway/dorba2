<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>DORBA Admin</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <META HTTP-EQUIV="Pragma" CONTENT="no-cache">
    <META HTTP-EQUIV="Expires" CONTENT="-1">

    <!-- Le styles -->
    <link href="../assets/css/bootstrap.css" rel="stylesheet">
    <style>
        body {
            padding-top: 60px; /* 60px to make the container go all the way to the bottom of the topbar */
        }
    </style>
    <link href="../assets/css/bootstrap-responsive.css" rel="stylesheet">
    <link type="text/css" href="../assets/css/ui-lightness/jqui.css" rel="stylesheet" />
    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <!-- Le fav and touch icons -->
    <link rel="shortcut icon" href="../assets/ico/favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="../assets/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="../assets/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="../assets/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="../assets/ico/apple-touch-icon-57-precomposed.png">
</head>

<body>
<?php
    include('../Connections/conn.php');

    include('navbar.html');
?>


<div class="container-fluid">
    <div class="row">
        <div id="main_container" class="row-fluid">
            <?php include('navmenu.html'); ?>
            <div class="span6">
                <div>
                    <h4>Overall Totals:</h4>
                    <table class="table table-striped">
                        <thead>
                        <th>Membership Type</th><th>Count</th>
                        </thead>
                        <?php


                        $hoursSQL = "Call GetCurrentMemberTotals();";
                        $query = $hoursSQL;

                        $results = $mysqli->multi_query($query);
                        $cnt = 0;
                        do {
                            /* store first result set */
                            if ($result = $mysqli->use_result()) {
                                while ($row = $result->fetch_array()) {
                                    if($cnt == 0){
                                        echo "<tr><td>Indv Memberships:</td><td> ".$row[0]."</td></tr>";
                                    } elseif($cnt == 1){
                                        echo "<tr><td>Family Memberships: </td><td>".$row[0]."</td></tr>";
                                    } elseif($cnt == 2){
                                        echo "<tr><td>Junior Memberships:</td><td> ".$row[0]."</td></tr>";
                                    } else {
                                        echo "<tr><td>Corp Memberships:</td><td> ".$row[0]."</td></tr>";
                                    }
                                }
                                $result->close();
                                $cnt++;
                            }

                        } while ($mysqli->next_result());
                        ?>
                    </table>
                </div>
                <div>
                       <h4>December Totals:</h4>
                    <table class="table table-striped">
                        <thead>
                        <th>Membership Type</th><th>Count</th>
                        </thead>
                        <?php

                        $decSQL = "Call GetMonthlyMemberTotals('2012-11-30','2013-01-01');";
                        $query = $decSQL;

                        $results = $mysqli->multi_query($query);
                        $cnt = 0;
                        do {
                            /* store first result set */
                            if ($result = $mysqli->use_result()) {
                                while ($row = $result->fetch_array()) {
                                    if($cnt == 0){
                                        echo "<tr><td>Indv Memberships:</td><td> ".$row[0]."</td></tr>";
                                    } elseif($cnt == 1){
                                        echo "<tr><td>Family Memberships: </td><td>".$row[0]."</td></tr>";
                                    } elseif($cnt == 2){
                                        echo "<tr><td>Junior Memberships:</td><td> ".$row[0]."</td></tr>";
                                    } else {
                                        echo "<tr><td>Corp Memberships:</td><td> ".$row[0]."</td></tr>";
                                    }
                                }
                                $result->close();
                                $cnt++;
                            }

                        } while ($mysqli->next_result());
                        ?>
                    </table>
                </div>
                <div>
                    <h4>November Totals:</h4>
                    <table class="table table-striped">
                        <thead>
                        <th>Membership Type</th><th>Count</th>
                        </thead>
                        <?php

                        $novSQL = "Call GetMonthlyMemberTotals('2012-10-31','2012-12-01');";
                        $query = $novSQL;

                        $results = $mysqli->multi_query($query);
                        $cnt = 0;
                        do {
                            /* store first result set */
                            if ($result = $mysqli->use_result()) {
                                while ($row = $result->fetch_array()) {
                                    if($cnt == 0){
                                        echo "<tr><td>Indv Memberships:</td><td> ".$row[0]."</td></tr>";
                                    } elseif($cnt == 1){
                                        echo "<tr><td>Family Memberships: </td><td>".$row[0]."</td></tr>";
                                    } elseif($cnt == 2){
                                        echo "<tr><td>Junior Memberships:</td><td> ".$row[0]."</td></tr>";
                                    } else {
                                        echo "<tr><td>Corp Memberships:</td><td> ".$row[0]."</td></tr>";
                                    }
                                }
                                $result->close();
                                $cnt++;
                            }

                        } while ($mysqli->next_result());
                        ?>
                    </table>
                </div>
            </div>

        </div>

    </div> <!-- /container -->
    <footer class="footer offset2"></footer>

    <!-- Le javascript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="../assets/js/jquery.js"></script>
    <script type="text/javascript" src="../jquery/jquery-ui-1.8.23.custom.min.js"></script>
    <script src="../assets/js/bootstrap.js"></script>
    <script src="js/members.js"></script>
</body>
</html>