<?php
include("session.php");
?>
<?php
	include("../Connections/conn.php"); 
	
	$query_catRS = "CALL GetArticles(0);";
	$catRS = mysql_query($query_catRS, $conn) or die(mysql_error());
	$row_CatRS = mysql_fetch_assoc($catRS);
	$totalRows_channelRS = mysql_num_rows($catRS);
	
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>DORBA Admin</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    	<META HTTP-EQUIV="Pragma" CONTENT="no-cache">
	<META HTTP-EQUIV="Expires" CONTENT="-1">

     <link href="../assets/css/bootstrap.css" rel="stylesheet">
    <link type="text/css" href="../assets/css/ui-lightness/jquery-ui-1.8.23.custom.css" rel="stylesheet" />
    <style>
      body {
        padding-top: 60px; /* 60px to make the container go all the way to the bottom of the topbar */
      }
    </style>
    <link href="../assets/css/bootstrap-responsive.css" rel="stylesheet">

    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <!-- Le fav and touch icons -->
    <link rel="shortcut icon" href="../assets/ico/favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="../assets/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="../assets/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="../assets/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="../assets/ico/apple-touch-icon-57-precomposed.png">
  </head>

  <body>

  <?php include('navbar.html'); ?>

    <div class="container-fluid">
    	<div class="row">
	    	<div id="main_container" class="row-fluid">
                <?php include('navmenu.html'); ?>
			<div class="span9" id="middle">
			<a id="addEvent" class="btn btn-success btn-mini">Add Article</a>
				<table class="table table-striped table-bordered table-condensed" id="eventList_tbl">
					<thead>
					  <tr>
					    <th></th>
					    <th>Title</th>
					    <!--
<th>Dates</th>
					    <th>On Home Page</th>
-->
					  </tr>
					</thead>
					<tbody id="traillistBody">
						<?php do { ?> 
			              <tr><td width="140"><!-- <a href="#" class="btn btn-primary btn-mini" onclick="showImages(<?php echo $row_CatRS['id'];?>);"><i class="icon-picture icon-white"/></a> --> <a href="#" onclick="editArticle(<?php echo $row_CatRS['articleId'];?>);" class="btn btn-success btn-mini"><i class="icon-edit"></i></a>  <a class="btn btn-mini btn-danger" onclick="deleteArticle(<?php echo $row_CatRS['articleId']; ?>);"><i class="icon-remove-sign icon-white"></i></a></td><td><a href="article.php?e=<?php echo $row_CatRS['articleId']; ?>"><?php echo $row_CatRS['articleTitle']; ?></a></td>
			           <?php } while ($row_CatRS = mysql_fetch_assoc($catRS)); mysql_free_result($catRS); ?>
					</tbody>
				</table>
			</div>
			<div id="newEventFrm" style="display:none;">
		    	<div class="modal-header" id="hdr">New Article</div>
		    	<div>
		    		<form class="form-horizontal" id="newevent_frm">
		    			<input type="hidden" name="articleid" id="articleid" value="0"/>
					  <div class="control-group">
					    <label class="control-label" for="title">Article Title</label>
					    <div class="controls">
					      <input type="text" id="title" name="title" placeholder="">
					    </div>
					  </div>
					  
					  <div class="control-group">
					    <label class="control-label" for="atype">Article Type</label>
					    <div class="controls">
					      <select name="atype">
					      	<option value="1">From the President</option>
					      </select>
					    </div>
					   
					 <div class="control-group">
					    <textarea class="jquery_ckeditor" cols="80" id="editor1" name="editor1" rows="10"></textarea>
					  </div>
					</form>
		    	</div>
		    	<div class="modal-footer">
			    	<a href="#" id="saveEvent" class="btn btn-success">Save</a>
		    	</div>
		    </div>
	    </div>
      
    </div> <!-- /container -->
    <footer class="footer offset2"></footer>
    <div class="modal hide fade" id="eventfiles_modal">
    	<div class="modal-header">
    		Event Files<button type="button" class="close" aria-hidden="true" onclick="closeFiles()">×</button>
    	</div>
    	<div class="modal-body">
    	
    	</div>
    	<div class="modal-footer">
    		<a href="#" class="btn btn-success btn-mini pull-right">Done</a>
    	</div>
    </div>
    
    <!-- Le javascript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script type="text/javascript" src="../jquery/jquery-1.8.0.min.js"></script>
	<script type="text/javascript" src="../jquery/jquery-ui-1.8.23.custom.min.js"></script>
    <script type="text/javascript" src="ckeditor/ckeditor.js"></script>
    <script type="text/javascript" src="ckeditor/adapters/jquery.js"></script>
    <script src="../js/ajaxfileupload.js"></script>
    <script src="../assets/js/bootstrap-transition.js"></script>
    <script src="../assets/js/bootstrap-alert.js"></script>
    <script src="../assets/js/bootstrap-modal.js"></script>
    <script src="../assets/js/bootstrap-dropdown.js"></script>
    <script src="../assets/js/bootstrap-scrollspy.js"></script>
    <script src="../assets/js/bootstrap-tab.js"></script>
    <script src="../assets/js/bootstrap-tooltip.js"></script>
    <script src="../assets/js/bootstrap-popover.js"></script>
    <script src="../assets/js/bootstrap-button.js"></script>
    <script src="../assets/js/bootstrap-collapse.js"></script>
    <script src="../assets/js/bootstrap-carousel.js"></script>
    <script src="../assets/js/bootstrap-typeahead.js"></script>
    <!-- <script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?key=AIzaSyBxbb7btwaq4ADW_y5cqQlM5GV2s3pyYB0&sensor=false"></script> -->
    <script src="../js/admin/articles.js"></script>

  </body>
</html>
