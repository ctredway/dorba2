<?php
    include("session.php");
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>DORBA Admin</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
	<META HTTP-EQUIV="Pragma" CONTENT="no-cache">
	<META HTTP-EQUIV="Expires" CONTENT="-1">

    <!-- Le styles -->
    <link href="../assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="../assets/css/bootstrap-responsive.min.css" rel="stylesheet">
      <link type="text/css" href="../assets/css/ui-lightness/jqui.css" rel="stylesheet" />
    <!-- <link href="site.css" rel="stylesheet"> -->
    <style type="text/css">
      body {
        padding-top: 60px;
        padding-bottom: 40px;
      }
      .sidebar-nav {
        padding: 9px 0;
      }
      
    </style>


    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <!-- Le fav and touch icons -->
    <link rel="shortcut icon" href="../assets/ico/favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="../assets/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="../assets/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="../assets/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="../assets/ico/apple-touch-icon-57-precomposed.png">
  </head>

  <body>

<?php include('navbar.html'); ?>
    	<!--
<ul class="breadcrumb">
		  <li>
		    <a href="index2.php">Home</a> <span class="divider">/</span>
		  </li>
		  <li class="active">
		    <a >Trails</a> 
		  </li>
		</ul>
-->
      <div class="container-fluid">
      <div class="row">
	    <div id="main_container" class="row-fluid">
	  		<?php include('navmenu.html'); ?>
			<div class="span9">
				<a id="addTrail" class="btn btn-success btn-mini" onclick="newTrail();">Add Trail</a>
                <a id="addTrailSteward" class="btn btn-primary btn-mini" onclick="showTrailSteward();">Add Steward</a>
				<table class="table table-striped table-bordered table-condensed" id="trailList_tbl">
					<thead>
					  <tr>
					    <th></th>
					    <th>Name</th>
					    <th>Location</th>
					    <th>Condition</th>
					  </tr>
					</thead>
					<tbody id="traillistBody"></tbody>
				</table>
			</div>
      
	    </div>
      </div>
    </div> <!-- /container -->
    
    <div id="trailFrm" class="modal hide fade">
    	<div class="modal-header">Trail
	    	<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    	</div>
    	<div class="modal-body">
    		<form class="form-horizontal" id="trail_frm">
    			<input type="hidden" name="trailId" id="trailId" value="0"/>
			  <div class="control-group">
			    <label class="control-label" for="trailName">Trail Name</label>
			    <div class="controls">
			      <input type="text" id="trailName" name="trailName" placeholder="">
			    </div>
			  </div>
			  <div class="control-group">
			    <label class="control-label" for="trailAddy">Address</label>
			    <div class="controls">
			      <input type="text" id="trailAddy" name="trailAddy" placeholder="">
			    </div>
			  </div>
			  <div class="control-group">
			    <label class="control-label" for="trailCity">City</label>
			    <div class="controls">
			      <input type="text" id="trailCity" name="trailCity" placeholder="">
			    </div>
			  </div>
			  <div class="control-group">
			    <label class="control-label" for="landOwner">Land Owner</label>
			    <div class="controls">
			      <input type="text" id="landOwner" name="landOwner" placeholder="">
			    </div>
			  </div>
			  <!--
			  <div class="control-group">
			    <label class="control-label" for="trailMap">Map</label>
			    <div class="controls">
			      <input type="text" id="trailMap" placeholder="">
			    </div>
			  </div>
			  -->
			  <div class="control-group">
			    <label class="control-label" for="facebook">Facebook</label>
			    <div class="controls">
			      <input type="text" id="facebook" name="facebook" placeholder="">
			    </div>
			  </div>
			  <div class="control-group">
			    <label class="control-label" for="twitter">Twitter</label>
			    <div class="controls">
			      <input type="text" id="twitter" name="twitter" placeholder="">
			    </div>
			  </div>
			  <div class="control-group">
			    <label class="control-label" for="trailStatus">Trail Status:</label>
			    <div class="controls">
					<select name="trailStatus" id="trailStatus"><option value="0">Closed</option><option value="1">Open</option></select>
			    </div>
			  </div>
				<div class="control-group">
				    <label class="control-label" for="conditions">Trail Condition:</label>
				    <div class="controls">
						<select name="conditions" id="conditions"></select>
				    </div>
				</div>
			  <div class="control-group">
			    <textarea class="jquery_ckeditor" cols="80" id="editor1" name="editor1" rows="10"></textarea>
			  </div>
			</form>
    	</div>
    	<div class="modal-footer">
	    	<div class="alert pull-left" id="onsave" style="display:none"></div><a href="#" id="saveTrail" class="btn btn-success">Save</a>
    	</div>
    </div>
    <div id="addSteward" class="modal fade hide">
        <div class="modal-header">Trail
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        </div>
        <div class="modal-body">
            <form class="form-horizontal" id="trail_ts_frm">
                <input type="hidden" name="sid" id="sid">
                <div class="control-group">
                    <label class="control-label" for="trail">Trail:</label>
                    <div class="controls">
                        <select name="trail" id="trail"></select>
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label" for="steward">Steward:</label>
                    <div class="controls">
                        <input type="text" id="steward" name="steward" class="input-medium" placeholder="">
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label" for="stewardType">Steward Type:</label>
                    <div class="controls">
                        <select name="stewardType" id="stewardType"><option value="1">Head Steward</option> <option value="2">Asst Steward</option></select>
                    </div>
                </div>
            </form>
        </div>
        <div class="modal-footer">
            <div class="alert pull-left" id="ontssave" style="display:none"></div><a href="#" id="saveTs" class="btn btn-success">Save</a>
        </div>
    </div>
    <!-- Le javascript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="../assets/js/jquery.js"></script>
    <script type="text/javascript" src="../jquery/jquery-ui-1.8.23.custom.min.js"></script>
    <script type="text/javascript" src="ckeditor/ckeditor.js"></script>
    <script type="text/javascript" src="ckeditor/adapters/jquery.js"></script>
    
    <script src="../assets/js/bootstrap.min.js"></script>

    <script src="js/trails.js"></script>

  </body>
</html>
