<?php include("header.php") ?>
<?php 
	
//include("Connections/conn.php"); 
	
	if(isset($_REQUEST['s'])){
		$query_catRS = "CALL GetSponsors(".$_REQUEST['s'].");";
		$catRS = mysql_query($query_catRS, $conn) or die(mysql_error());
		$row_CatRS = mysql_fetch_assoc($catRS);
		$totalRows_channelRS = mysql_num_rows($catRS);
		if($totalRows_channelRS == 0){
			header( 'Location: sponsors.php' ) ;
		}
	} else {
		header( 'Location: sponsors.php' ) ;
	}
	
?> 

    <div class="container-fluid">
    
      <div class="row-fluid content_buffer">
        
        <div class="span9 offset1">

        	<div class="row-fluid">
	          <div class="well" id="sponsor_txt">
                  <div class="page-header">
                      <h2><?php echo $row_CatRS['sponsorName']; ?> <a href="#" id="edit_sponsor" class="btn btn-success btn-small" style="display: none">Edit</a> </h2>
                      <?php if($row_CatRS['sponsorUrl'] != ''){ echo '<a href="http://'.$row_CatRS['sponsorUrl'].'" target="_blank">Visit Website</a>';} ?>
                  </div>
	            <?php echo $row_CatRS['sponsorDesc']; ?>
	          </div>
              <div id="sponsor_edit" style="display: none">
                <form id="sponsor_edit_frm">
                    <input type="hidden" name="sid" value="<?php echo $row_CatRS['sponsorId']; ?>">
                     <input type="text" name="sponsorName" id="sponsorName" value="<?php echo $row_CatRS['sponsorName']; ?>" placeholder="Sponsor Name">
                    <input type="text" name="website" id="website" value="<?php echo $row_CatRS['sponsorUrl']; ?>" placeholder="Sponsor Website">
                    <input type="text" name="contact" id="contact" value="<?php echo $row_CatRS['contact']; ?>" placeholder="Contact Number/Address">
                    <textarea id="sponsor_html" name="editor1"></textarea>
                    <a href="#" id="cancelEdit" class="btn btn-small btn-danger">Cancel</a>
                    <a href="#" id="saveEdit" class="btn btn-small btn-success">Save</a>
                </form>
              </div>
        	</div>
          
        </div><!--/span-->
      
<!--        <div class="span3">-->
<!--          <div class="well sidebar-nav">-->
<!--          	<h4>Locations</h4>-->
<!--            <div id="smallMap"></div>-->
<!--            <div><address><?php //echo $row_CatRS['trailAddress']; ?></address></div>-->
<!--        </div>-->
        </div><!--/span-->
      </div><!--/row-->

      <hr>

      <footer>
        <p>&copy; DORBA 2012</p>
      </footer>

    </div><!--/.fluid-container-->
    
    <!-- Le javascript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="js/json2.js"></script>
    <script src="assets/js/jquery.js"></script>
    <script type="text/javascript" src="admin/ckeditor/ckeditor.js"></script>
    <script type="text/javascript" src="admin/ckeditor/adapters/jquery.js"></script>
    <script src="assets/js/bootstrap.js"></script>
    <script src="js/login.js"></script>
    <!-- <script src="js/app.js"></script> -->
    <!-- <script src="js/trails.js"></script> -->
    <script src="js/sponsor.js"></script>
  </body>
</html>
