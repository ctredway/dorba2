    <?php include("header.php") ?>

    <div class="container offset1 span10">
    
      <div class="row content_buffer">
        
        <div>

        	<div class="row" id="main">
        		<div class="well">
                    <h3>DORBA Sponsors</h3>
        		The following businesses show their support for DORBA and it's mission to provide great trails and great events by providing benefits to DORBA members.
<br/><br/>
For Sponsors, the program is simple and gives you access to an audience of over 4,000 local cyclists If you are interested in becoming a DORBA Trail Sponsor or otherwise supporting DORBA's mission of land access and trail building click <a href="contact.php">HERE</a>
 for more information and to apply to become DORBA sponsor.
        		</div>
                <div id="spList"></div>
<!--	          <div class="well" id="spmap">-->
<!--	            -->
<!--	          </div>-->
        	</div>
          <div class="row-fluid" id="edit" style="display: none">

          </div><!--/row-->
        </div><!--/span-->
      </div><!--/row-->

      <hr>

      <footer>
        <p>&copy; DORBA 2012</p>
      </footer>

    </div><!--/.fluid-container-->
    
    <!-- Le javascript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="js/json2.js"></script>
    <script src="assets/js/jquery.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
    <script src="js/underscore.js"></script>
    <script src="js/backbone.js"></script>

    <script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?key=AIzaSyBxbb7btwaq4ADW_y5cqQlM5GV2s3pyYB0&sensor=false&libraries=weather"></script>
    <!-- <script src="js/app.js"></script> -->
    <script src="js/login.js"></script>
    <script src="js/sponsors.js"></script>
  </body>
</html>
