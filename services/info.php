<?php 
	date_default_timezone_set('America/Chicago');

	$currentDate = date("Y-m-d");// current date
	
	//Display the current date
	echo $currentDate."<br>";
	
	//Add one year to current date
	$dateOneYearAdded = strtotime(date("Y-m-d", strtotime($currentDate)) . " +1 year");
	echo date('Y-m-d', $dateOneYearAdded)."<br>";
?>