<?php
	error_reporting(E_ERROR);
	include("../Connections/conn.php");
	$trail=0;
	
	if(isset($_REQUEST['t'])){
		$trail = $_REQUEST['t'];
	}

	//if a 0 is passed all trails will be returned
	$query_cg = 'CALL GetTrails('.$trail.');';
	$trailsRS = $mysqli->query($query_cg);

	$trails = array();

	while ($row = $trailsRS->fetch_assoc()){
	    $trails[] = array('trail'=>$row);
	};

	echo json_encode(array('trails'=>$trails));
    $trailsRS->close();

?>